#pragma once

#include <algorithm>
#include <concepts>
#include <cstddef>
#include <iostream>
#include <numeric>
#include <ranges>
#include <string>
#include <type_traits>

namespace polus::buffer {

template <bool Mutable> struct __base_buffer_types;

template <> struct __base_buffer_types<true> {
	using reference_type = char &;
	using iterator = char *;
};

template <> struct __base_buffer_types<false> {
	using reference_type = const char &;
	using iterator = const char *;
};

template <bool Mutable> class basic_buffer;

template <typename T> struct is_buffer : std::false_type {};
template <bool Mutable>
struct is_buffer<basic_buffer<Mutable>> : std::true_type {};

template <typename T>
concept buffer_c = is_buffer<T>::value;

template <buffer_c A, buffer_c B>
struct buffer_iter_compatible : std::false_type {};

template <bool B>
struct buffer_iter_compatible<basic_buffer<B>, basic_buffer<B>>
	: std::true_type {};

template <>
struct buffer_iter_compatible<basic_buffer<true>, basic_buffer<false>>
	: std::true_type {};

template <typename Self, typename Other>
concept buffer_iter_compatible_c = buffer_iter_compatible<Self, Other>::value;

template <typename It, typename Buff>
concept to_iterator_convertible =
	std::integral<It> || std::convertible_to<It, typename Buff::iterator>;

enum case_sensitivity {
	case_sensitive,
	case_insensitive
};

constexpr char tolower(char c) { return c > 0x40 && c < 0x5b ? c + 0x20 : c; }

struct buffer_equals {
	template <typename A, typename B>
	constexpr bool operator()(const A &a, const B &b) {
		return std::ranges::equal(a, b);
	}
};

template <std::unsigned_integral Int> struct fnv_constants {};
template <> struct fnv_constants<std::uint32_t> {
	constexpr static std::uint32_t fnv_prime = 0x01000193ul,
								   fnv_offset = 0x811c9dc5ul;
};
template <> struct fnv_constants<std::uint64_t> {
	constexpr static std::uint64_t fnv_prime = 0x00000100000001B3ull,
								   fnv_offset = 0xcbf29ce484222325ull;
};

struct buffer_hash {
	template <typename T> constexpr std::size_t operator()(const T &a) {
		std::size_t hash = fnv_constants<std::size_t>::fnv_offset;
		for (char c : a)
			hash = (hash ^ c) * fnv_constants<std::size_t>::fnv_prime;
		return hash;
	}
};

template <bool Mutable> class basic_buffer {
public:
	constexpr static bool is_mutable = Mutable;
	using char_type = char;
	using traits_type = std::char_traits<char_type>;
	using __depandant_types = __base_buffer_types<is_mutable>;
	using reference_type = typename __depandant_types::reference_type;
	using iterator = typename __depandant_types::iterator;

	constexpr basic_buffer(iterator begin, iterator end) noexcept
		: _begin(begin), _end(end) {}
	constexpr basic_buffer(iterator pos, std::size_t size) noexcept
		: _begin(pos), _end(pos + size) {}
	constexpr basic_buffer(std::nullptr_t) noexcept
		: _begin(nullptr), _end(nullptr) {}
	constexpr basic_buffer() noexcept : basic_buffer(nullptr) {}

	template <buffer_iter_compatible_c<basic_buffer> _Other>
	constexpr basic_buffer(const _Other &other) noexcept
		: basic_buffer(other.begin(), other.end()) {}
	constexpr basic_buffer(const char_type *literal) noexcept
		requires(!is_mutable)
		: basic_buffer(literal, std::char_traits<char>::length(literal)) {}

	constexpr iterator begin() const noexcept { return _begin; }
	constexpr iterator end() const noexcept { return _end; }
	constexpr std::size_t size() const noexcept {
		return static_cast<std::size_t>(std::distance(begin(), end()));
	}
	constexpr auto at(std::size_t idx) const noexcept { return begin()[idx]; }
	constexpr auto operator[](std::size_t idx) const noexcept {
		return at(idx);
	}
	constexpr bool operator==(std::nullptr_t) const noexcept {
		return begin() == nullptr || end() == nullptr;
	}

	constexpr basic_buffer substr(
		to_iterator_convertible<basic_buffer> auto begin,
		to_iterator_convertible<basic_buffer> auto end) const noexcept {
		return __substr(__to_it(begin), __to_it(end));
	}

	constexpr basic_buffer
	shift(to_iterator_convertible<basic_buffer> auto begin) const noexcept {
		return __shift(__to_it(begin));
	}

	constexpr basic_buffer
	shrink(to_iterator_convertible<basic_buffer> auto end) const noexcept {
		return __shrink(__to_it(end));
	}

	constexpr basic_buffer get_passed(basic_buffer start) const noexcept {
		return *this == nullptr ? nullptr
								: basic_buffer(start.begin(), begin());
	}

	template <buffer_c _Target>
	constexpr iterator find(_Target target) const noexcept {
		/* Inspired by the glibc's std::string_view::find */
		if (target.size() == 0) return begin();
		std::size_t length = size();
		for (iterator it = begin(); length >= target.size();) {
			it = traits_type::find(
				it, length - target.size() + 1, *target.begin());
			if (it == nullptr) break;
			if (traits_type::compare(it, target.begin(), target.size()) == 0)
				return it;
			length = static_cast<std::size_t>(end() - ++it);
		}
		return end();
	}

	constexpr iterator find(char_type target) const noexcept {
		return std::find(begin(), end(), target);
	}

	template <bool M2>
	requires(Mutable) basic_buffer operator<<(basic_buffer<M2> in) const {
		return in.size() > size()
				   ? nullptr
				   : shift(std::copy(in.begin(), in.end(), begin()));
	}

	template <int Dummy = 0>
	requires(Mutable) basic_buffer operator<<(char_type in) const {
		if (1 > size()) return nullptr;
		*begin() = in;
		return basic_buffer(begin() + 1, end());
	}

private:
	constexpr basic_buffer
	__substr(iterator begin, iterator end) const noexcept {
		if (*this == nullptr || begin > end || begin < this->begin() ||
			end > this->end())
			return nullptr;
		return basic_buffer(begin, end);
	}
	constexpr basic_buffer __shift(iterator begin) const noexcept {
		return substr(begin, end());
	}
	constexpr basic_buffer __shrink(iterator end) const noexcept {
		return substr(begin(), end);
	}

	constexpr iterator __to_it(iterator it) const noexcept { return it; }
	constexpr iterator __to_it(std::integral auto n) const noexcept {
		return begin() + n;
	}

	iterator _begin;
	iterator _end;
}; // namespace polus::buffer

namespace literals {

using mutable_buffer = ::polus::buffer::basic_buffer<true>;
using const_buffer = ::polus::buffer::basic_buffer<false>;

constexpr const_buffer operator""_cb(const char *literal, std::size_t) {
	return const_buffer(literal);
}

template <buffer_c _Buff>
inline std::ostream &operator<<(std::ostream &stream, _Buff buff) {
	return stream.write(buff.begin(), buff.size());
}

} // namespace literals

} // namespace polus::buffer

namespace polus {
using namespace buffer::literals;
}

template <>
inline constexpr bool std::ranges::enable_borrowed_range<polus::const_buffer> =
	true;
