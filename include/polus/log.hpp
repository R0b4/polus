#pragma once

#include <iostream>
#include <mutex>
#include <polus/fiber/core.hpp>

namespace polus {

inline std::mutex __log_mutex;
template <typename... Args> void log(Args &&...args) {
	std::lock_guard __lock(__log_mutex);
	std::cout << '[' << fib::current_fiber() << "] ";
	((std::cout << std::forward<Args>(args)), ...);
	std::cout << std::endl;
}

} // namespace polus
