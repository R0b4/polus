#pragma once

#include <chrono>
#include <polus/buffer.hpp>
#include <polus/ids.hpp>
#include <polus/time.hpp>
#include <polus/utils/expected.hpp>

namespace polus::os {

struct file;

file *open_tcp_listener(const char *service);
file *tcp_connect(const char *peer, const char *service);
std::pair<file *, file *> make_pipe();

constexpr std::size_t file_closed = 0;
constexpr std::size_t would_block = std::numeric_limits<std::size_t>::max();

os::file *accept(os::file *listener);
std::size_t read(file *, mutable_buffer);
std::size_t write(file *, const_buffer);

void close(file *);

using events_mask = std::uint16_t;
enum event : events_mask {
	event_read = 0x1,
	event_write = 0x2,
	event_close = 0x4,
	event_err = 0x8,
	event_abort = 0x10
};
constexpr events_mask no_events = 0;

struct poll;
using poll_id_type = base_id_type<poll>;

constexpr bool poll_edge_triggered = true;

poll *make_poll();

void poll_add(poll *, file *, poll_id_type);
void poll_report_ioblock(poll *, poll_id_type, events_mask);
void poll_remove(poll *, file *, poll_id_type);

std::pair<poll_id_type, events_mask> poll_get_event(poll *);
void poll_wait_until(poll *, time_point_type time);
inline void poll_wait_for(poll *p, duration_c auto time) {
	poll_wait_until(p, clock_type::now() + time);
}
void poll_wait(poll *p);

void close_poll(poll *);

} // namespace polus::os
