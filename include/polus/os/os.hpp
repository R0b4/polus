#pragma once

#if defined(WIN32) || defined(_WIN32) || \
	defined(__WIN32) && !defined(__CYGWIN__)
#define POLUS_WINDOWS
#elif defined(__unix__)
#define POLUS_UNIX
#if defined(__linux__) || defined(linux)
#define POLUS_LINUX
#endif
#endif
