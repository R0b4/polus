#pragma once

#include <memory>
#include <polus/buffer.hpp>

namespace polus {

class dynamic_buffer_core {
public:
	dynamic_buffer_core(
		std::size_t max_size, std::size_t initial_capacity = 1000);
	constexpr std::size_t max_size() const noexcept { return _max_size; }

protected:
	dynamic_buffer_core() = default;
	std::size_t _max_size;
	std::size_t _capacity;
	std::unique_ptr<mutable_buffer::char_type[]> _buffer;
	mutable_buffer _data;
};

class dynamic_buffer_read : virtual public dynamic_buffer_core {
public:
	const_buffer data() const noexcept { return _data; }
	void consume(std::size_t n) noexcept;
};

class dynamic_buffer_write : virtual public dynamic_buffer_core {
public:
	mutable_buffer get_push_block(std::size_t n) noexcept;
	void push(std::size_t n) noexcept;
	bool push(const_buffer b) noexcept;
};

class dynamic_buffer : public dynamic_buffer_read, public dynamic_buffer_write {
public:
	dynamic_buffer(std::size_t max_size, std::size_t initial_capacity = 1000);
};

} // namespace polus
