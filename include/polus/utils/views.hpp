#include <ranges>

namespace polus {

/* copies a variadic amount of views to the output iteraror */
template <std::weakly_incrementable OutIt, std::ranges::input_range... Ranges>
requires(std::indirectly_copyable<std::ranges::iterator_t<Ranges>, OutIt>
			 &&...) inline OutIt copy_views(OutIt it, Ranges... ranges);

template <std::weakly_incrementable OutIt> inline OutIt copy_views(OutIt it) {
	return it;
}
template <
	std::ranges::input_range FirstRange, std::ranges::input_range... Ranges,
	std::weakly_incrementable OutIt>
requires(
	std::indirectly_copyable<std::ranges::iterator_t<FirstRange>, OutIt> &&
	(std::indirectly_copyable<std::ranges::iterator_t<Ranges>, OutIt> &&
	 ...)) inline OutIt
	copy_views(OutIt it, FirstRange first, Ranges... ranges) {
	std::ranges::copy(first, it);
	return copy_views(std::forward<OutIt>(it), std::forward<Ranges>(ranges)...);
}

template <std::ranges::forward_range _Str, std::ranges::forward_range _Del>
inline std::ranges::iterator_t<_Str> find_delimiter(_Str &&str, _Del &&del) {
	for (auto sit = str.begin(); sit != str.end(); ++sit)
		if (std::equal(del.begin(), del.end(), sit, str.end())) return sit;
	return str.end();
}

template <
	std::ranges::input_range _Str, std::ranges::forward_range _Del,
	std::ranges::output_range<std::ranges::range_value_t<_Str>> _Out>
requires(std::ranges::forward_range<_Out>) inline std::ranges::in_out_result<
	std::ranges::iterator_t<_Str>,
	std::ranges::iterator_t<
		_Out>> find_delimiter_and_copy(_Str &&str, _Del &&del, _Out &&out) {
	auto strpos = str.begin();
	auto outpos = out.begin();
	for (auto dit = del.begin();
		 dit != del.end() && strpos != str.end() && outpos != out.end();) {
		*outpos++ = *strpos++;
		if (++dit == del.end()) break;
	}
	for (auto compare_pos = out.begin();
		 outpos != out.end() &&
		 !std::equal(compare_pos, outpos, del.begin(), del.end()) &&
		 strpos != str.end();
		 ++compare_pos) {
		*outpos++ = *strpos;
		if (std::equal(compare_pos, outpos, del.begin(), del.end())) break;
		++strpos;
	}
	return {std::move(strpos), std::move(outpos)};
}

} // namespace polus
