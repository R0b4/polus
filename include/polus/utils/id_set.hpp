#pragma once

#include <queue>

namespace polus {

template <typename I> class id_set {
public:
	using id_type = I;

	inline id_set() noexcept;
	inline id_set(id_set &&) noexcept;
	inline id_set &operator=(id_set &&) noexcept;

	inline std::size_t size() const noexcept;
	inline bool empty() const noexcept;

	inline id_type get() noexcept;
	inline void remove(id_type) noexcept;

private:
	id_type _size;
	std::queue<id_type> _free;
};

} // namespace polus

template <typename I> inline polus::id_set<I>::id_set() noexcept : _size(0) {}

template <typename I>
inline polus::id_set<I>::id_set(id_set &&obj) noexcept
	: _size(obj._size), _free(std::move(obj._free)) {}

template <typename I>
inline polus::id_set<I> &polus::id_set<I>::operator=(id_set &&obj) noexcept {
	this->~IdSet();
	new (this) id_set(std::move(obj));
	return *this;
}

template <typename I>
inline std::size_t polus::id_set<I>::size() const noexcept {
	return _size - _free.size();
}

template <typename I> inline bool polus::id_set<I>::empty() const noexcept {
	return _size == _free.size();
}

template <typename I>
inline typename polus::id_set<I>::id_type polus::id_set<I>::get() noexcept {
	if (_free.empty()) return _size++;
	auto id = _free.front();
	_free.pop();
	return id;
}

template <typename I>
inline void polus::id_set<I>::remove(id_type id) noexcept {
	_free.push(id);
}
