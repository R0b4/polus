#pragma once

#include <concepts>
#include <cstddef>
#include <memory>

namespace polus {

template <typename Func> class move_only_function;

template <typename Ret, typename... Args>
class move_only_function<Ret(Args...)> {
public:
	move_only_function() noexcept = default;
	move_only_function(std::nullptr_t) noexcept : move_only_function() {}
	move_only_function(const move_only_function &) = delete;
	move_only_function(move_only_function &&) noexcept = default;
	template <std::invocable<Args...> Func>
	requires std::convertible_to<std::invoke_result_t<Func, Args...>, Ret>
	move_only_function(Func &&func)
		: _func(std::make_unique<func_impl<Func>>(std::forward<Func>(func))) {}

	move_only_function &operator=(move_only_function &&) noexcept = default;
	move_only_function &operator=(const move_only_function &) = delete;
	move_only_function &operator=(std::nullptr_t) {
		_func = nullptr;
		return *this;
	}
	template <std::invocable<Args...> Func>
	requires std::convertible_to<std::invoke_result_t<Func, Args...>, Ret>
		move_only_function &operator=(Func &&func) {
		_func = std::make_unique<func_impl<Func>>(std::forward<Func>(func));
		return *this;
	}

	void swap(move_only_function &other) noexcept { _func.swap(other._func); }

	explicit operator bool() const noexcept { return _func != nullptr; }

	Ret operator()(Args &&...args) {
		return _func->call(std::forward<Args>(args)...);
	}

	~move_only_function() = default;

	friend bool
	operator==(const move_only_function &f, std::nullptr_t) noexcept {
		return f._func == nullptr;
	}

private:
	struct ifunc {
		virtual Ret call(Args &&...args) = 0;
		virtual ~ifunc() = default;
	};

	template <typename F> struct func_impl : ifunc {
		func_impl(F &&func) : func(std::move(func)) {}
		Ret call(Args &&...args) override {
			return func(std::forward<Args>(args)...);
		}
		~func_impl() = default;
		F func;
	};

	std::unique_ptr<ifunc> _func;
};

}; // namespace polus

namespace std {
template <typename Func>
void swap(
	polus::move_only_function<Func> &a, polus::move_only_function<Func> &b) {
	a.swap(b);
}
} // namespace std
