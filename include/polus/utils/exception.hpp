#pragma once

#include <exception>
#include <system_error>

namespace polus {
struct polus_exception : public std::exception {};

struct polus_system_exception : public std::system_error {
	polus_system_exception(std::error_code __ec, const std::string &__what)
		: std::system_error(__ec, __what) {}
	~polus_system_exception() noexcept override {}
};

[[noreturn]] inline void throw_errno_err(int errcode = errno) {
	auto err = std::error_code(errcode, std::system_category());
	throw polus_system_exception(err, err.message());
}

} // namespace polus
