#pragma once

#include <utility>

namespace polus {

template <typename _Fn, typename... Args>
inline auto bind(_Fn &&func, Args &&...args) {
	return [func = std::forward<_Fn>(func),
			... args = std::forward<Args>(args)](auto &&...args2) mutable {
		return func(
			std::forward<Args>(args)...,
			std::forward<decltype(args2)>(args2)...);
	};
}

} // namespace polus
