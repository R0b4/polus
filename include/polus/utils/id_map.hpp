#pragma once

#include <utility>

namespace polus {

template <typename I, typename T> class id_map {
public:
	using id_type = I;
	using value_type = T;

private:
	struct _Elem {
		id_type stack_val;
		bool occupied;
		value_type val;
	};

public:
	class iterator {
	public:
		using difference_type = std::ptrdiff_t;
		using value_type = std::pair<I, T>;
		iterator(id_map *obj, _Elem *ptr) : pos(ptr), obj(obj) {}
		T operator*() const {
			return std::make_pair<const I, T &>(
				static_cast<I>(pos - obj->elements), pos->val);
		}
		iterator &operator++() {
			auto end = obj->elements + obj->capacity;
			while (++pos < end && !pos->occupied)
				;
			return *this;
		}
		iterator operator++(int) {
			auto self = *this;
			++*this;
			return self;
		}
		bool operator==(iterator other) const { return pos == other.pos; }

	private:
		_Elem *pos;
		id_map *obj;
	};

	class const_iterator {
	public:
		using difference_type = std::ptrdiff_t;
		using value_type = std::pair<const I, const T &>;
		const_iterator(const id_map *obj, const _Elem *ptr)
			: pos(ptr), obj(obj) {}
		T operator*() const {
			return std::make_pair<const I, const T &>(
				static_cast<I>(pos - obj->elements), pos->val);
		}
		iterator &operator++() {
			auto end = obj->elements + obj->capacity;
			while (++pos < end && !pos->occupied)
				;
			return *this;
		}
		iterator operator++(int) {
			auto self = *this;
			++*this;
			return self;
		}
		bool operator==(iterator other) const { return pos == other.pos; }

	private:
		const _Elem *pos;
		const id_map *obj;
	};
	friend iterator;
	friend const_iterator;

	id_map(id_type size_hint = 100) noexcept;
	id_map(id_map &&) noexcept;
	id_map &operator=(id_map &&) noexcept;

	id_type add(value_type &&) noexcept;
	id_type add(const value_type &);
	template <typename... Args> id_type emplace(Args &&...args);

	iterator begin() { return ++iterator(this, elements - 1); }
	iterator end() { return iterator(this, elements + capacity); }
	const_iterator begin() const {
		return ++const_iterator(this, elements - 1);
	}
	const_iterator end() const {
		return const_iterator(this, elements + capacity);
	}

	void erase(id_type) noexcept;

	std::size_t size() const noexcept;
	bool empty() const noexcept;

	value_type &at(id_type);
	const value_type &at(id_type) const;

	value_type &operator[](id_type);
	const value_type &operator[](id_type) const;

	~id_map();

private:
	inline id_type get_space() noexcept;

	_Elem *elements;
	id_type capacity;
	id_type stack_size;
	id_type _size;
};

} // namespace polus

// Implementation:

namespace polus {
template <typename I, typename T>
inline id_map<I, T>::id_map(id_type size_hint) noexcept
	: capacity(size_hint), stack_size(0), _size(0) {
	elements = static_cast<_Elem *>(::operator new(size_hint * sizeof(_Elem)));
	for (id_type i = 0; i < size_hint; i++) { elements[i].occupied = false; }
}

template <typename I, typename T>
inline id_map<I, T>::id_map(id_map &&obj) noexcept
	: elements(std::exchange(obj.elements, nullptr)),
	  capacity(std::exchange(obj.capacity, 0)),
	  stack_size(std::exchange(obj.stack_size, 0)),
	  _size(std::exchange(obj._size, 0)) {}

template <typename I, typename T>
inline id_map<I, T> &id_map<I, T>::operator=(id_map &&obj) noexcept {
	this->~id_map();
	elements = std::exchange(obj.elements, nullptr);
	stack_size = std::exchange(obj.stack_size, 0);
	capacity = std::exchange(obj.capacity, 0);
	_size = std::exchange(obj._size, 0);
	return *this;
}

template <typename I, typename T>
inline typename polus::id_map<I, T>::id_type
id_map<I, T>::get_space() noexcept {
	if (stack_size) return elements[--stack_size].stack_val;
	else if (_size != capacity)
		return _size++;

	id_type ncap = capacity * 2;
	_Elem *nelems = static_cast<_Elem *>(::operator new(ncap * sizeof(_Elem)));
	if (nelems == nullptr) std::terminate();
	for (id_type i = 0; i < capacity; i++) {
		nelems[i].occupied = true;
		::new (&nelems[i].val) value_type(std::move(elements[i].val));
		nelems[i].stack_val = elements[i].stack_val;
	}
	for (id_type i = capacity; i < ncap; i++) nelems[i].occupied = false;
	::operator delete(static_cast<void *>(elements));
	elements = nelems;
	capacity = ncap;
	return _size++;
}

template <typename I, typename T>
inline typename id_map<I, T>::id_type
id_map<I, T>::add(value_type &&obj) noexcept {
	auto id = get_space();
	::new (&elements[id].val) value_type(std::move(obj));
	elements[id].occupied = true;
	return id;
}

template <typename I, typename T>
inline typename id_map<I, T>::id_type id_map<I, T>::add(const value_type &obj) {
	auto id = get_space();
	::new (&elements[id].val) value_type(obj);
	elements[id].occupied = true;
	return id;
}

template <typename I, typename T>
template <typename... Args>
inline typename id_map<I, T>::id_type id_map<I, T>::emplace(Args &&...args) {
	auto id = get_space();
	::new (&elements[id].val) value_type(std::forward<Args>(args)...);
	elements[id].occupied = true;
	return id;
}

template <typename I, typename T>
inline void id_map<I, T>::erase(id_type id) noexcept {
	std::destroy_at(&elements[id].val);
	elements[id].occupied = false;
	elements[stack_size++].stack_val = id;
}

template <typename I, typename T>
inline std::size_t id_map<I, T>::size() const noexcept {
	return this->_size - this->stack_size;
}

template <typename I, typename T>
inline bool id_map<I, T>::empty() const noexcept {
	return size() == 0;
}

template <typename I, typename T>
inline typename id_map<I, T>::value_type &id_map<I, T>::at(id_type id) {
	return elements[id].val;
}

template <typename I, typename T>
inline const typename id_map<I, T>::value_type &
id_map<I, T>::at(id_type id) const {
	return elements[id].val;
}

template <typename I, typename T>
inline typename id_map<I, T>::value_type &id_map<I, T>::operator[](id_type id) {
	return at(id);
}

template <typename I, typename T>
inline const typename id_map<I, T>::value_type &
id_map<I, T>::operator[](id_type id) const {
	return at(id);
}

template <typename I, typename T> inline id_map<I, T>::~id_map() {
	if (!elements) return;

	std::for_each_n(elements, _size, [](auto &e) {
		if (e.occupied) std::destroy_at(&e.val);
	});
	::operator delete(static_cast<void *>(elements));
	elements = nullptr;
	stack_size = _size = capacity = 0;
}

} // namespace polus
