#pragma once

#include <utility>
#include <variant>

#ifdef __cpp_lib_expected
#include <expected>
#endif

namespace polus {

#ifdef __cpp_lib_expected

template <typename Err> using unexpected = std::unexpected<Err>;
using unexpect_t = std::unexpect_t;
constexpr unexpect_t unexpect{};
template <typename Val, typename Err> using expected = std::expected<Val, Err>;

#else

template <typename Err> class unexpected {
public:
	constexpr unexpected(const unexpected &e) = default;
	constexpr unexpected(unexpected &&) noexcept = default;
	constexpr explicit unexpected(Err &&e) : err(std::forward<Err>(e)) {}
	template <typename... Args>
	constexpr explicit unexpected(std::in_place_t, Args &&...args)
		: err(std::forward<Args>(args)...) {}
	template <typename U, typename... Args>
	constexpr explicit unexpected(
		std::in_place_t, std::initializer_list<U> il, Args &&...args)
		: err(il, std::forward<Args>(args)...) {}

	const Err &error() const & { return err; }
	Err &error() & { return err; }
	const Err &&error() const && { return std::move(err); }
	Err &&error() && { return std::move(err); }

	constexpr void
	swap(unexpected &other) noexcept(std::is_nothrow_swappable_v<Err>) {
		std::swap(err(), other.err());
	}

	template <typename Err2>
	friend constexpr bool operator==(unexpected &a, unexpected<Err2> &b) {
		return a.error() == b.error();
	}

private:
	Err err;
};

struct unexpect_t {
	explicit unexpect_t() = default;
};
constexpr unexpect_t unexpect{};

template <typename Val, typename Err> class [[nodiscard]] expected {
public:
	using value_type = Val;
	using error_type = Err;

	constexpr expected() : _val(value_type()) {}
	constexpr expected(expected &&) noexcept = default;
	constexpr expected(const expected &) = default;

	template <typename OVal, typename OErr>
	constexpr expected(const expected<OVal, OErr> &other)
		: _val(other.has_value() ? other.value() : other.error()) {}

	template <typename OVal, typename OErr>
	constexpr expected(expected<OVal, OErr> &&other)
		: _val(std::move(other.has_value() ? other.value() : other.error())) {}

	template <typename Val2>
	constexpr explicit(!std::is_convertible_v<Val2, value_type>)
		expected(Val2 &&value)
		: _val(std::in_place_type<value_type>, std::forward<Val2>(value)) {}

	template <typename Err2>
	constexpr explicit(!std::is_convertible_v<const Err2 &, error_type>)
		expected(const unexpected<Err2> &e)
		: _val(std::in_place_type<error_type>, e.error()) {}

	template <typename Err2>
	constexpr explicit(!std::is_convertible_v<Err2, error_type>)
		expected(unexpected<Err2> &&e)
		: _val(std::in_place_type<error_type>, std::move(e.error())) {}

	template <typename... Args>
	constexpr expected(std::in_place_t, Args &&...args)
		: _val(std::in_place_type<value_type>, std::forward<Args>(args)...) {}

	template <typename U, typename... Args>
	constexpr expected(
		std::in_place_t, std::initializer_list<U> il, Args &&...args)
		: _val(
			  std::in_place_type<value_type>, il, std::forward<Args>(args)...) {
	}

	template <typename... Args>
	constexpr expected(unexpect_t, Args &&...args)
		: _val(std::in_place_type<error_type>, std::forward<Args>(args)...) {}

	template <typename U, typename... Args>
	constexpr expected(unexpect_t, std::initializer_list<U> il, Args &&...args)
		: _val(
			  std::in_place_type<error_type>, il, std::forward<Args>(args)...) {
	}

	constexpr expected &operator=(const expected &other) {
		this->~expected();
		new (this) expected(other);
		return *this;
	}
	constexpr expected &operator=(expected &&other) {
		this->~expected();
		new (this) expected(std::move(other));
		return *this;
	}
	template <typename Val2 = value_type>
	constexpr expected &operator=(Val2 &&other) {
		this->~expected();
		new (this) expected(std::forward<Val2>(other));
		return *this;
	}
	template <typename Err2>
	constexpr expected &operator=(const unexpected<Err2> &other) {
		this->~expected();
		new (this) expected(other);
		return *this;
	}
	template <typename Err2>
	constexpr expected &operator=(unexpected<Err2> &&other) {
		this->~expected();
		new (this) expected(std::move(other));
		return *this;
	}

	constexpr const value_type *operator->() const noexcept { return &**this; }
	constexpr value_type *operator->() noexcept { return &**this; }
	constexpr const value_type &operator*() const &noexcept { return value(); }
	constexpr value_type &operator*() &noexcept { return value(); }
	constexpr const value_type &&operator*() const &&noexcept {
		return std::move(value());
	}
	constexpr value_type &&operator*() &&noexcept { return std::move(value()); }

	constexpr bool has_value() const noexcept { return _val.index() == 0; }
	explicit operator bool() const noexcept { return has_value(); }

	constexpr value_type &value() & { return std::get<value_type>(_val); }
	constexpr const value_type &value() const & {
		return std::get<value_type>(_val);
	}
	constexpr value_type &&value() && {
		return std::move(std::get<value_type>(_val));
	}
	constexpr const value_type &&value() const && {
		return std::move(std::get<error_type>(_val));
	}

	constexpr const error_type &error() const &noexcept {
		return std::get<error_type>(_val);
	}
	constexpr error_type &error() &noexcept {
		return std::get<error_type>(_val);
	}
	constexpr const error_type &&error() const &&noexcept {
		return std::move(std::get<error_type>(_val));
	}
	constexpr error_type &&error() &&noexcept {
		return std::move(std::get<error_type>(_val));
	}

	template <typename U>
	constexpr value_type value_or(U &&default_value) const & {
		return has_value()
				   ? value()
				   : static_cast<value_type>(std::forward<U>(default_value));
	}
	template <typename U> constexpr value_type value_or(U &&default_value) && {
		return has_value()
				   ? std::move(value())
				   : static_cast<value_type>(std::forward<U>(default_value));
	}

	template <typename... Args> value_type &emplace(Args &&...args) {
		return _val.template emplace<value_type>(std::forward<Args>(args)...);
	}
	template <typename U, typename... Args>
	value_type &emplace(std::initializer_list<U> il, Args &&...args) {
		return _val.template emplace<value_type>(
			il, std::forward<Args>(args)...);
	}

	constexpr void swap(expected &other) { std::swap(_val, other._val); }

	template <typename Val2, typename Err2>
	friend constexpr bool operator==(const expected &a, const expected &b) {
		if (a.has_value() != b.has_value) return false;
		if (a.has_value()) {
			return a.value() == b.value();
		} else {
			return a.error() == b.error();
		}
	}
	template <typename Val2>
	friend constexpr bool operator==(const expected &e, const Val2 &val) {
		return e.has_value() ? e.value() == val : false;
	}
	template <typename Err2>
	friend constexpr bool
	operator==(const expected &e, const unexpected<Err2> &val) {
		return e.has_value() ? false : e.error() == val.error();
	}

private:
	std::variant<value_type, error_type> _val;
};

#endif

} // namespace polus

#ifndef __cpp_lib_expected
namespace std {

template <typename Err>
void swap(polus::unexpected<Err> &a, polus::unexpected<Err> &b) {
	a.swap(b);
}

template <typename Val, typename Err>
void swap(polus::expected<Val, Err> &a, polus::expected<Val, Err> &b) {
	a.swap(b);
}

} // namespace std
#endif
