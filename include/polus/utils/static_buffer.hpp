#pragma once

#include <polus/buffer.hpp>

namespace polus {

template <std::size_t Size> class static_buffer : public mutable_buffer {
public:
	constexpr static_buffer() noexcept
		: mutable_buffer(std::begin(space), std::end(space)) {}

private:
	char space[Size];
};

} // namespace polus
