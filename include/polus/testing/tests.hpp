#pragma once

namespace polus::testing {

void test_filebufs();
void test_debugbufs();
void test_http();

void run_all_tests() noexcept;

} // namespace polus::testing
