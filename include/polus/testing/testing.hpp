#pragma once

#include <iostream>
#include <string>
#include <typeinfo>

namespace polus::testing {

struct test_failed_exception : std::exception {
	test_failed_exception(const char *file, const char *line_str, int line)
		: file(file), line(line), line_str(line_str){};
	const char *file;
	int line;
	const char *line_str;
};

struct sub_test_failed : std::exception {};

inline void
test_assert(bool b, const char *file, const char *line_str, int line_nr) {
	if (!b) { throw test_failed_exception(file, line_str, line_nr); }
}
#define POLUS_ASSERT(B) test_assert((B), __FILE__, #B, __LINE__)

inline std::string __tabs;

template <std::invocable Func>
inline void run_test(const char *name, Func &&func) {
	std::cerr << __tabs << "TESTING: " << name << ':' << std::endl;
	try {
		__tabs.push_back('\t');
		try {
			func();
			__tabs.pop_back();
		} catch (...) {
			__tabs.pop_back();
			throw;
		}
	} catch (test_failed_exception &e) {
		std::cerr << __tabs << "TEST: " << name << ": "
				  << "FAILED"
				  << "\r\n\t" << __tabs << "assertion \"" << e.line_str
				  << "\" failed\r\n\t" << __tabs << e.file << ':' << e.line
				  << std::endl;
		throw sub_test_failed();
	} catch (sub_test_failed &e) {
		std::cerr << __tabs << "TEST: " << name << ": "
				  << "FAILED" << std::endl;
		throw sub_test_failed();
	} catch (std::exception &e) {
		std::cerr << __tabs << "TEST: " << name << ": "
				  << "FAILED"
				  << "\r\n\t" << __tabs << "Exception was thrown of type "
				  << typeid(e).name() << ": " << e.what() << std::endl;
		throw sub_test_failed();
	}
}

struct __polus_test_macro_left {
	__polus_test_macro_left(const char *name) : name(name){};
	const char *name = name;
	template <std::invocable Func>
	friend void operator<<(__polus_test_macro_left _macroleft, Func &&func) {
		return run_test(_macroleft.name, std::forward<Func>(func));
	}
};

#define POLUS_TEST(name) polus::testing::__polus_test_macro_left(name) << [&]()
#define POLUS_TESTF(name, func) polus::testing::run_test(name, func)

} // namespace polus::testing
