#pragma once

#include <polus/io/io_buf.hpp>

namespace polus::testing {

struct __internal_shared_buf_state_part;
struct __internal_shared_buf_state;

class internal_shared_buf : public io_buf {
public:
	internal_shared_buf(
		int bufidx, std::shared_ptr<__internal_shared_buf_state> state);

	void read_abort() override;
	void write_abort() override;

	std::size_t read(mutable_buffer) override;
	std::size_t read_until(mutable_buffer, time_point_type) override;

	std::size_t write(const_buffer) override;
	std::size_t write_until(const_buffer, time_point_type) override;

	~internal_shared_buf() override;

private:
	__internal_shared_buf_state_part &my_state();
	__internal_shared_buf_state_part &other_state();

	int _bufidx;
	std::shared_ptr<__internal_shared_buf_state> _state;
};

std::pair<
	std::unique_ptr<internal_shared_buf>, std::unique_ptr<internal_shared_buf>>
make_shared_buf_pair();

} // namespace polus::testing
