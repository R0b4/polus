#pragma once

#include <cstdint>
#include <numeric>

namespace polus {

template <
	typename based, std::unsigned_integral Int = std::uint64_t,
	Int Invalid = std::numeric_limits<Int>::max()>
class base_id_type {
public:
	using integer_primitive = Int;
	base_id_type() = default;
	constexpr base_id_type(integer_primitive value) : _value(value) {}
	constexpr operator integer_primitive &() & { return _value; }
	constexpr operator const integer_primitive &() const & { return _value; }
	constexpr operator integer_primitive &&() && { return std::move(_value); }
	constexpr operator const integer_primitive &&() const && {
		return std::move(_value);
	}
	static constexpr base_id_type invalid() {
		return base_id_type(invalid_id_primitive);
	}

private:
	constexpr static integer_primitive invalid_id_primitive = Invalid;
	integer_primitive _value;
};

} // namespace polus

namespace std {

template <typename based, typename Int, Int Invalid>
struct hash<polus::base_id_type<based, Int, Invalid>> {
	std::size_t
	operator()(const polus::base_id_type<based, Int, Invalid> &obj) const {
		return std::hash<Int>()(obj);
	}
};

} // namespace std
