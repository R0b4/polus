#pragma once

#include <memory>
#include <polus/io/socket_buf.hpp>

namespace polus {

struct listener_error : polus_exception {};
struct listener_abort_error : listener_error {};

class listener {
public:
	virtual std::unique_ptr<socket_buf> accept() = 0;
	virtual std::unique_ptr<socket_buf> accept_until(time_point_type) = 0;
	std::unique_ptr<socket_buf> accept_for(const duration_c auto &duration) {
		return accept_for(clock_type::now() + duration);
	}

	virtual void abort_accept() = 0;
	virtual ~listener() = default;
};

}; // namespace polus
