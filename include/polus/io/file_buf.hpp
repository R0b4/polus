#pragma once

#include <polus/fiber/mutex.hpp>
#include <polus/io/io_buf.hpp>
#include <polus/os/file.hpp>

namespace polus {

struct filebuf_io_error : io_buf_error {};

class file_buf : public io_buf {
public:
	file_buf(os::file *f);

	void read_abort() override;
	void write_abort() override;

	std::size_t read(mutable_buffer) override;
	std::size_t read_until(mutable_buffer, time_point_type) override;

	std::size_t write(const_buffer) override;
	std::size_t write_until(const_buffer, time_point_type) override;

	~file_buf() override;

private:
	os::file *_file;
	os::poll_id_type _id;
	fib::mutex _evs_mut;
	os::events_mask _evs;
	bool read_aborted, write_aborted;
};

} // namespace polus
