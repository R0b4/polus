#pragma once

#include <polus/io/listener.hpp>

namespace polus {

struct tcplistener_closed : listener_error {};
struct tcplistener_io_error : listener_error {};

class tcp_listener : public listener {
public:
	tcp_listener(const char *service);
	std::unique_ptr<socket_buf> accept() override;
	std::unique_ptr<socket_buf> accept_until(time_point_type) override;
	void abort_accept() override;
	~tcp_listener() override;

private:
	os::file *_sock;
	os::poll_id_type _id;
	os::events_mask _evs;
	fib::mutex _evs_mut;
};

} // namespace polus
