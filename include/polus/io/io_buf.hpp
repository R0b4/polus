#pragma once

#include <polus/fiber/mutex.hpp>
#include <polus/io/socket_buf.hpp>
#include <polus/os/file.hpp>
#include <polus/utils/static_buffer.hpp>

namespace polus {

struct io_buf_error : socket_buf_error {};

class io_buf : public socket_buf {
public:
	io_buf() noexcept;
	virtual ~io_buf() = default;

	constexpr static std::size_t in_buffer_size = 20000;
	constexpr static std::size_t out_buffer_size = 20000;

protected:
	std::streamsize xsgetn(char_type *s, std::streamsize count) override;
	std::streamsize xsputn(const char_type *s, std::streamsize count) override;

	int_type underflow() override;
	int_type overflow(int_type ch = traits_type::eof()) override;
	int sync() override;

	virtual std::size_t read(mutable_buffer) = 0;
	virtual std::size_t read_until(mutable_buffer, time_point_type) = 0;

	virtual std::size_t write(const_buffer) = 0;
	virtual std::size_t write_until(const_buffer, time_point_type) = 0;

private:
	static_buffer<in_buffer_size> _in_buffer;
	static_buffer<out_buffer_size> _out_buffer;
};

} // namespace polus
