#pragma once

#include <polus/buffer.hpp>
#include <streambuf>

namespace polus {

class better_streambuf : public std::streambuf {
public:
	/// @brief gets the put area of the streambuf
	mutable_buffer put_area();

	/// @brief updates the put area of the streambuf
	/// @param  pptr the start of the new put area
	/// @return the new put area
	mutable_buffer put_area(mutable_buffer::iterator);

	/// @brief gets the get area of the streambuf
	const_buffer get_area();

	/// @brief updates the get area of the streambuf
	/// @param  pptr the start of the get put area
	/// @return the new get area
	const_buffer get_area(const_buffer::iterator);
};

} // namespace polus
