#pragma once

#include <polus/buffer.hpp>
#include <polus/io/better_streambuf.hpp>
#include <polus/utils/exception.hpp>
#include <streambuf>

namespace polus {

struct socket_buf_error : polus_exception {};
struct socket_aborted_error : socket_buf_error {};
struct socket_timeout_error : socket_buf_error {};

class socket_buf : public better_streambuf {
public:
	virtual void read_abort() = 0;
	virtual void write_abort() = 0;

	constexpr bool is_secure() const noexcept { return _is_secure; }

	virtual ~socket_buf() = default;

protected:
	bool _is_secure = true;
};

} // namespace polus
