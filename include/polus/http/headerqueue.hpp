#pragma once

#include <polus/http/http.hpp>
#include <polus/utils/dynamic_buffer.hpp>

namespace polus {

class header_queue {
public:
	header_queue(std::size_t maximum_headers_length = 10000);
	bool add(header);
	bool add(const_buffer name, const_buffer value) {
		return add(header(name, value));
	}
	header try_read();

private:
	dynamic_buffer _buf;
};

} // namespace polus
