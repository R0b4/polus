#pragma once

#include <polus/buffer.hpp>
#include <polus/ids.hpp>
#include <polus/io/socket_buf.hpp>

namespace polus {

struct header {
	constexpr explicit header(
		const_buffer name = nullptr, const_buffer value = nullptr) noexcept
		: name(name), value(value){};

	constexpr static header err() { return header(); }

	constexpr header setname(const_buffer name) const noexcept {
		return header(name, value);
	}
	constexpr header setvalue(const_buffer value) const noexcept {
		return header(name, value);
	}

	static header from_line(const_buffer line);

	const_buffer name;
	const_buffer value;

	constexpr bool operator==(std::nullptr_t) const noexcept {
		return name == nullptr || value == nullptr;
	}
};

enum class http_version {
	v10,
	v11,
	v2,
	v3
};

struct __status;
using status_code_type = std::uint16_t;

struct http_error : polus_exception {};

class http_buf : public socket_buf {
public:
	virtual header try_read_header() = 0;

	virtual bool write_top(
		http_version, status_code_type, const_buffer status_text = nullptr) = 0;
	virtual bool
	write_top(const_buffer method, const_buffer path, http_version) = 0;
	virtual bool write_header(header) = 0;
	bool emplace_header(const_buffer name, const_buffer value) {
		return write_header(header(name, value));
	}

	virtual void close_write_side() = 0;
};

} // namespace polus
