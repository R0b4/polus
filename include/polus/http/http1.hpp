#pragma once

#include <polus/http/headerqueue.hpp>
#include <polus/http/http.hpp>
#include <polus/utils/expected.hpp>
#include <polus/utils/static_buffer.hpp>

namespace polus {

struct http1_past_top_phase : http_error {};
struct http1_did_not_send_top : http_error {};
struct http1_line_too_long : http_error {};

class http1_buf : public http_buf {
public:
	http1_buf(socket_buf &socket, bool server_side);

	header try_read_header() override;

	bool write_top(
		http_version, status_code_type,
		const_buffer status_text = nullptr) override;
	bool
	write_top(const_buffer method, const_buffer path, http_version) override;
	bool write_header(header) override;
	void close_write_side() override;

	void read_abort() override;
	void write_abort() override;

protected:
	std::streamsize xsgetn(char_type *s, std::streamsize count) override;
	std::streamsize xsputn(const char_type *s, std::streamsize count) override;

	int_type overflow(int_type) override;
	int_type underflow() override;
	int sync() override;

private:
	bool __sync();

	bool write_end(bool);
	bool read_end(bool);

	template <bool Trailer> bool write_headers(bool &, const_buffer);
	bool write_chunked(bool &, const_buffer);
	bool write_serial(bool &, const_buffer);

	bool __rsync();

	bool read_line(const_buffer &line);
	bool read_top();
	bool read_header();
	bool read_serial(bool &done);
	bool read_chunk_size();
	bool read_chunk(bool &done);
	bool read_chunk_crlf();
	bool read_last_chunk_crlf();
	void check_trailer();
	bool read_trailer();

	bool read_chunk_to(mutable_buffer &buf);
	bool read_serial_to(mutable_buffer &buf);

	socket_buf *_socket;

	bool _server_side;

	enum class write_status {
		top,
		headers,
		body_serial,
		body_chunked,
		trailer,
		end
	};
	write_status _wstatus;
	bool _closed_write_side;
	header_queue _wheaders;
	bool _whas_body, _wchunked, _wtrailer;
	std::size_t _wsize;
	static_buffer<20000> _wbuffer;

	enum class read_status {
		top,
		headers,
		body_chunk_size,
		body_chunked,
		body_chunk_end_crlf,
		body_last_chunk_crlf,
		body_serial,
		check_trailer,
		trailer,
		end
	};
	read_status _rstatus;
	header_queue _rheaders;
	static_buffer<20000> _rbuffer;
	bool _rhas_body, _rchunked, _rtrailer;
	std::size_t _rnext_size;
};

} // namespace polus
