#pragma once

#include <polus/time.hpp>

namespace polus::fib {

void sleep_until(const time_point_type &time);
inline void sleep_for(const duration_c auto &duration) {
	return sleep_until(clock_type::now() + duration);
}

} // namespace polus::fib
