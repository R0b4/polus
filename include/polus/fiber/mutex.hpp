#pragma once

#include <atomic>
#include <mutex>
#include <polus/fiber/core.hpp>
#include <queue>

namespace polus::fib {

class mutex {
public:
	void lock();
	bool try_lock();
	void unlock();

private:
	bool _locked = false;
	std::mutex _mut;
	std::queue<fiber> _fibs;
};

}; // namespace polus::fib
