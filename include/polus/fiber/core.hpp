#pragma once

#include <boost/context/fiber.hpp>
#include <future>
#include <polus/ids.hpp>
#include <polus/utils/bind.hpp>
#include <polus/utils/moveonly_function.hpp>

namespace polus::fib {

namespace ctx = boost::context;

struct __fiber_id;
using fiber_id_type = base_id_type<__fiber_id>;
using fiber = std::pair<ctx::fiber, fiber_id_type>;

/// @brief used to manage the scheduling of all fibers.
class scheduler {
public:
	/// @brief tasks the scheduler with scheduling the given fiber to be run.
	/// @param ready
	virtual void notify_ready(fiber &&ready) = 0;

	/// @brief adds the fiber to its list of scheduled fibers.
	/// @param fiber
	/// @return
	virtual fiber make_scheduled_fiber(ctx::fiber &&fiber) = 0;
	virtual void remove_fiber(fiber_id_type) = 0;

	/// @brief checks if there are fibers which are waiting to be run. Used by
	/// force_switch to not make an unnecessary context switch.
	/// @return
	virtual bool has_ready_fibers() = 0;

	/// @brief waits until a fiber which needs to be run is ready and returns it
	/// or if the scheduler runs out of fibers it returns an empty fiber marking
	/// the end of the program.
	/// @return a fiber ready to be run.
	virtual fiber get_fiber() = 0;

	/// @brief the given function will be run at the end of the program. This
	/// can be for example used to shutdown some background service threads.
	/// @param func
	virtual void add_exit_handler(move_only_function<void()> &&func) = 0;
	virtual ~scheduler() = default;
};

void set_scheduler(std::unique_ptr<scheduler> &&scheduler);
template <typename T, typename... Args>
inline void set_scheduler(Args &&...args) {
	return set_scheduler(std::make_unique<T>(std::forward<Args>(args)...));
}

/// @brief the given function will be run at the end of the program. This
/// can be for example used to shutdown some background service threads.
/// @param func
void add_exit_handler(move_only_function<void()> &&func);

/// @brief gives the numerical id of the fiber which calls it. (like calling
/// std::this_thread::get_id() but for fibers).
/// @return
fiber_id_type current_fiber();

/// @brief tasks the scheduler with scheduling the given fiber to be run.
/// @param fib
void set_ready(fiber &&fib);

/// @brief spawns a new fiber running the given function.
/// @param func the function to be run
/// @param stack_size the size of the allocated call stack.
void spawn(move_only_function<void()> func, std::size_t stack_size = 8000);

/// @brief stops a given fiber handing of execution to another fiber
/// @param handle_self this function will be called with the current fibers
/// handle you need to place the handle somewhere to wait until further
/// execution.
void yield(move_only_function<void(fiber &&)> &&handle_self);

/// @brief hands of execution to another fiber if there is one waiting. the
/// current fiber will immediately be added to the scheduling queue. This
/// function is used to more evenly distribute computational resources among
/// fibers.
void force_switch();

/// @brief this function will run fibers which are scheduled to be run.
void fiber_thread_loop();

} // namespace polus::fib
