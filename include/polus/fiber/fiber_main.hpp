#pragma once

#include <polus/fiber/core.hpp>
#include <polus/fiber/scheduler.hpp>
#include <thread>

namespace polus::fib {

inline const std::size_t default_thread_count =
	std::thread::hardware_concurrency();

/// @brief default starting point for a program using these fibers. It starts
/// threads for running fibers, and starts background services.
/// @tparam ...Args
/// @tparam Func
/// @param func the starting point of you program. This function will be started
/// as the first fiber.
/// @param ...args these arguments will be passed to func.
template <typename... Args, std::invocable<Args...> Func>
inline void run_fiber_main(Func &&func, Args &&...args) {
	fib::set_scheduler<fib::default_scheduler>();
	std::vector<std::jthread> threads;
	threads.emplace_back(polus::fib::events_wait_loop);
	threads.emplace_back(polus::fib::condition_timer);
	fib::async(std::forward<Func>(func), std::forward<Args>(args)...);
	std::generate_n(std::back_inserter(threads), default_thread_count, []() {
		return std::jthread(fib::fiber_thread_loop);
	});
}

/// @brief default starting point for a program using these fibers. It starts
/// threads for running fibers, and starts background services.
/// @tparam ...Args
/// @tparam Func
/// @param stack_size the size of the allocated callstack for the first fiber.
/// @param func the starting point of you program. This function will be started
/// as the first fiber.
/// @param ...args these arguments will be passed to func.
template <typename... Args, std::invocable<Args...> Func>
inline void
run_fiber_main(std::size_t stack_size, Func &&func, Args &&...args) {
	fib::set_scheduler<fib::default_scheduler>();
	std::vector<std::jthread> threads;
	threads.emplace_back(polus::fib::events_wait_loop);
	threads.emplace_back(polus::fib::condition_timer);
	fib::async(
		stack_size, std::forward<Func>(func), std::forward<Args>(args)...);
	std::generate_n(std::back_inserter(threads), default_thread_count, []() {
		return std::jthread(fib::fiber_thread_loop);
	});
}

} // namespace polus::fib
