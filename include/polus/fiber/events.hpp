#pragma once

#include <polus/fiber/condition_variable.hpp>
#include <polus/fiber/mutex.hpp>
#include <polus/os/file.hpp>

namespace polus::fib {

os::poll_id_type register_file(os::file *);

constexpr bool events_edge_triggered = os::poll_edge_triggered;

void events_report_ioblock(os::poll_id_type id, os::events_mask evs);

os::events_mask events_wait(os::poll_id_type);
os::events_mask events_wait_until(os::poll_id_type, time_point_type);
inline os::events_mask
events_wait_for(os::poll_id_type id, const duration_c auto &duration) {
	return events_wait_until(id, clock_type::now() + duration);
}

os::events_mask
events_wait(os::poll_id_type, std::unique_lock<fib::mutex> &lock);
os::events_mask events_wait_until(
	os::poll_id_type, time_point_type, std::unique_lock<fib::mutex> &lock);
inline os::events_mask events_wait_for(
	os::poll_id_type id, const duration_c auto &duration,
	std::unique_lock<fib::mutex> &lock) {
	return events_wait_until(id, clock_type::now() + duration, lock);
}

void abort_wait(os::poll_id_type);
void remove_file(os::poll_id_type);
void events_wait_loop();

} // namespace polus::fib
