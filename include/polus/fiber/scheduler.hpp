#pragma once

#include <condition_variable>
#include <polus/fiber/core.hpp>
#include <polus/utils/id_set.hpp>
#include <queue>

namespace polus::fib {

class default_scheduler : public scheduler {
	void notify_ready(fiber &&ready) override;
	fiber make_scheduled_fiber(ctx::fiber &&fiber) override;
	fiber get_fiber() override;
	void remove_fiber(fiber_id_type) override;
	void add_exit_handler(move_only_function<void()> &&) override;
	bool has_ready_fibers() override;

public:
	std::mutex _mut;
	std::queue<fiber> fib_queue;
	std::condition_variable _cond;
	id_set<fiber_id_type> fibers;
	std::vector<move_only_function<void()>> exit_handlers;
};

} // namespace polus::fib
