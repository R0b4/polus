#pragma once

#include <chrono>
#include <condition_variable>
#include <list>
#include <map>
#include <polus/fiber/mutex.hpp>
#include <polus/time.hpp>
#include <unordered_map>

namespace polus::fib {

namespace __impl {
using fib_list = std::list<fiber>;
using fib_iterator = fib_list::iterator;
std::mutex &get_timing_scheduler_mutex();
void timing_scheduler_mutex_wait(
	time_point_type time, fib_iterator fib, fib_list &lst, std::mutex &sig_mut);
} // namespace __impl

/// @brief a condition_variable for fibers like std::condition_variable. To use
/// the timing functionanlity of this object polus::fib::condition_timer() must
/// be running in another thread. If you use polus::fib::fiber_main this will
/// already be the case.
class condition_variable {
public:
	void notify_one();
	void notify_all();

	template <typename Mutex> void wait(std::unique_lock<Mutex> &lock) {
		yield([&lock, this](fiber &&self) {
			std::lock_guard __guard(_waiting_mut);
			lock.unlock();
			_waiting.push_back(std::move(self));
		});
		lock.lock();
	}

	template <typename Mutex, std::predicate Pred>
	void wait(std::unique_lock<Mutex> &lock, Pred predicate) {
		while (!predicate()) wait(lock);
	}

	template <typename Mutex, duration_c Duration>
	std::cv_status
	wait_for(std::unique_lock<Mutex> &lock, const Duration &duration) {
		return wait_until(lock, std::chrono::steady_clock::now() + duration);
	}
	template <typename Mutex, duration_c Duration, std::predicate Pred>
	bool wait_for(
		std::unique_lock<Mutex> &lock, const Duration &duration,
		Pred predicate) {
		return wait_until(
			lock, std::chrono::steady_clock::now() + duration, predicate);
	}

	template <typename Mutex>
	std::cv_status
	wait_until(std::unique_lock<Mutex> &lock, const time_point_type &abs_time) {
		yield([&lock, abs_time, this](fiber &&self) {
			std::lock_guard<std::mutex> __lock(
				__impl::get_timing_scheduler_mutex());
			std::lock_guard __guard(_waiting_mut);
			lock.unlock();
			_waiting.push_back(std::move(self));
			__impl::timing_scheduler_mutex_wait(
				abs_time, std::prev(_waiting.end()), _waiting, _waiting_mut);
		});
		lock.lock();
		return std::cv_status::no_timeout;
	}

	template <typename Mutex, std::predicate Predicate>
	bool wait_until(
		std::unique_lock<Mutex> &lock, const time_point_type &abs_time,
		Predicate predicate) {
		while (!predicate())
			if (wait_until(lock, abs_time) == std::cv_status::timeout)
				return predicate();
		return true;
	}

	~condition_variable() { std::lock_guard __lock(_waiting_mut); }

private:
	std::mutex _waiting_mut;
	std::list<fiber> _waiting;
};

/// @brief A service function which must be running to use condition_variable's
/// timing functionality.
void condition_timer();

} // namespace polus::fib
