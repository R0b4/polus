#pragma once

#include <chrono>
#include <mutex>
#include <optional>
#include <polus/fiber/condition_variable.hpp>
#include <polus/utils/exception.hpp>
#include <polus/utils/moveonly_function.hpp>

namespace polus::fib {

struct polus_already_set_promise : polus_exception {};
struct polus_no_shared_state : polus_exception {};
struct polus_already_retrieved_future : polus_exception {};

template <class T> class furure_shared_state_value {
public:
	void set_exception(std::exception_ptr exc) { _exc = exc; }
	void set(const T &value) { _value = value; }
	void set(T &&value) { _value = std::move(value); }
	T get() {
		if (_exc.has_value()) std::rethrow_exception(*_exc);
		T res(std::move(*_value));
		return res;
	}
	bool has_value() const noexcept {
		return _exc.has_value() || _value.has_value();
	}

private:
	std::optional<std::exception_ptr> _exc;
	std::optional<T> _value;
};

template <class T> class furure_shared_state_value<T &> {
public:
	void set_exception(std::exception_ptr exc) { _exc = exc; }
	void set(T &value) { _value = &value; }
	T &get() const noexcept {
		if (_exc.has_value()) std::rethrow_exception(*_exc);
		return *_value;
	}
	bool has_value() const noexcept {
		return _exc.has_value() || _value.has_value();
	}

private:
	std::optional<std::exception_ptr> _exc;
	std::optional<T *> _value;
};

template <> class furure_shared_state_value<void> {
public:
	void set_exception(std::exception_ptr exc) { _exc = exc; }
	void set() { isset = true; }
	void get() const {
		if (_exc.has_value()) std::rethrow_exception(*_exc);
	}
	bool has_value() const noexcept { return _exc.has_value() || isset; }

private:
	std::optional<std::exception_ptr> _exc;
	bool isset = false;
};

template <typename T, typename... Args>
concept __future_to_shared_settable =
	requires(furure_shared_state_value<T> &val, Args &&...args) {
	{val.set(std::forward<Args>(args)...)};
};

template <class T> struct future_shared_state {
	fib::mutex mut;
	fib::condition_variable var;
	furure_shared_state_value<T> value;
};

template <typename T> class promise;

template <typename T> class future {
public:
	using value_type = T;

	future() noexcept = default;
	future(future &&) noexcept = default;
	future(const future &) = delete;

	future &operator=(future &&) noexcept = default;
	future &operator=(const future &) = delete;

	value_type get() {
		if (_state == nullptr) throw polus_no_shared_state{};
		std::unique_lock _lock(_state->mut);
		_state->var.wait(_lock, [this] { return _state->value.has_value(); });
		return _state->value.get();
	}

	bool valid() const noexcept { return _state != nullptr; }
	void wait() {
		if (_state == nullptr) throw polus_no_shared_state{};
		std::unique_lock _lock(_state->mut);
		_state->var.wait(_lock, [this] { return _state->value.has_value(); });
	}

	bool wait_for(const duration_c auto &timeout) const {
		return wait_until(clock_type::now() + timeout);
	}

	bool wait_until(const time_point_type &timeout) const {
		if (_state == nullptr) throw polus_no_shared_state{};
		std::unique_lock _lock(_state->mut);
		return _state->var.wait_until(
			_lock, timeout, [this] { return _state->value.has_value(); });
	}

private:
	friend class promise<value_type>;

	future(
		const std::shared_ptr<future_shared_state<value_type>> &state) noexcept
		: _state(state) {}
	future(std::shared_ptr<future_shared_state<value_type>> &&state) noexcept
		: _state(std::move(state)) {}
	std::shared_ptr<future_shared_state<value_type>> _state;
};

template <typename T> class promise {
public:
	using value_type = T;

	promise() noexcept
		: _state(std::make_shared<future_shared_state<value_type>>()){};
	promise(promise &&) noexcept = default;
	promise(const promise &) = delete;

	promise &operator=(promise &&) noexcept = default;
	promise &operator=(const promise &) = delete;

	void swap(promise &other) noexcept { _state.swap(other._state); }
	future<value_type> get_future() {
		retrieved_future = true;
		return future<value_type>(_state);
	}

	template <typename... Args>
	requires(__future_to_shared_settable<value_type, Args...>) void set_value(
		Args &&...value) {
		std::lock_guard _guard(_state->mut);
		if (_state->value.has_value()) throw polus_already_set_promise{};
		_state->value.set(std::forward<Args>(value)...);
		_state->var.notify_one();
	}

	void set_exception(std::exception_ptr exc) {
		std::lock_guard _guard(_state->mut);
		if (_state->value.has_value()) throw polus_already_set_promise{};
		_state->value.set_exception(exc);
		_state->var.notify_one();
	}

private:
	std::shared_ptr<future_shared_state<value_type>> _state;
	bool retrieved_future = false;
};

template <typename T> struct packaged_task;

template <typename T, typename Callable, typename... Args>
void __packaged_call_set_to_promise_help(
	promise<T> &promise, Callable &func, Args &&...args) {
	try {
		promise.set_value(func(std::forward<Args>(args)...));
	} catch (...) { promise.set_exception(std::current_exception()); }
}

template <typename Callable, typename... Args>
void __packaged_call_set_to_promise_help(
	promise<void> &promise, Callable &func, Args &&...args) {
	try {
		func(std::forward<Args>(args)...);
		promise.set_value();
	} catch (...) { promise.set_exception(std::current_exception()); }
}

template <typename Ret, typename... Args> struct packaged_task<Ret(Args...)> {
public:
	packaged_task() = default;
	packaged_task(packaged_task &&) noexcept = default;
	template <std::invocable<Args...> F>
	requires std::convertible_to<std::invoke_result_t<F, Args...>, Ret>
	packaged_task(F &&func) : _func(std::forward<F>(func)) {}

	packaged_task &operator=(packaged_task &&) noexcept = default;
	bool valid() const noexcept { return _func != nullptr; }

	void swap(packaged_task &other) noexcept { std::swap(*this, other); };
	void operator()(Args &&...args) {
		return __packaged_call_set_to_promise_help(
			_promise, _func, std::forward<Args>(args)...);
	}

	future<Ret> get_future() { return _promise.get_future(); }

	void reset() { _promise = promise<Ret>(); }

private:
	move_only_function<Ret(Args...)> _func;
	promise<Ret> _promise;
};

} // namespace polus::fib

namespace std {

template <typename T>
void swap(polus::fib::promise<T> &a, polus::fib::promise<T> &b) {
	return a.swap(b);
}

} // namespace std
