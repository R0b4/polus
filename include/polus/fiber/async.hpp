#pragma once

#include <polus/fiber/core.hpp>
#include <polus/fiber/future.hpp>

namespace polus::fib {

/// @brief spawns a new fiber which will be executed concurrently with existing
/// fibers.
/// @tparam ...Args
/// @tparam _Fn
/// @param stack_size the size of the callstack which will be allocated for this
/// fiber.
/// @param func the callable which will be start in the new fiber
/// @param ...args the arguments which will be passed to the given callable
/// @return	a future holding the result of the given callable.
template <typename... Args, std::invocable<Args...> _Fn>
inline polus::fib::future<std::invoke_result_t<_Fn, Args...>>
async(std::size_t stack_size, _Fn &&func, Args &&...args) {
	polus::fib::packaged_task<std::invoke_result_t<_Fn, Args...>()> task(
		polus::bind(std::forward<_Fn>(func), std::forward<Args>(args)...));
	auto future = task.get_future();
	spawn(std::move(task), stack_size);
	return future;
}

/// @brief spawns a new fiber which will be executed concurrently with existing
/// fibers.
/// @tparam ...Args
/// @tparam _Fn
/// @param func the callable which will be start in the new fiber
/// @param ...args the arguments which will be passed to the given callable
/// @return	a future holding the result of the given callable.
template <typename... Args, std::invocable<Args...> _Fn>
inline polus::fib::future<std::invoke_result_t<_Fn, Args...>>
async(_Fn &&func, Args &&...args) {
	polus::fib::packaged_task<std::invoke_result_t<_Fn, Args...>()> task(
		polus::bind(std::forward<_Fn>(func), std::forward<Args>(args)...));
	auto future = task.get_future();
	spawn(std::move(task));
	return future;
}

} // namespace polus::fib
