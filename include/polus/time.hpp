#pragma once

#include <chrono>
#include <type_traits>

namespace polus {
using clock_type = std::chrono::steady_clock;
using time_point_type = clock_type::time_point;
using duration_type = clock_type::duration;

template <typename T> struct is_duration : std::false_type {};
template <typename Rep, typename Period>
struct is_duration<std::chrono::duration<Rep, Period>> : std::true_type {};
template <typename T> constexpr bool is_duration_v = is_duration<T>::value;

template <typename T>
concept duration_c = is_duration_v<T>;

} // namespace polus
