#include <iostream>
#include <polus/utils/dynamic_buffer.hpp>

namespace polus {

dynamic_buffer_core::dynamic_buffer_core(
	std::size_t max_size, std::size_t initial_capacity)
	: _max_size(max_size), _capacity(initial_capacity),
	  _buffer(std::make_unique<mutable_buffer::char_type[]>(initial_capacity)),
	  _data(_buffer.get(), _buffer.get()) {}

void dynamic_buffer_read::consume(std::size_t n) noexcept {
	_data = _data.shift(n);
}

mutable_buffer dynamic_buffer_write::get_push_block(std::size_t n) noexcept {
	if (_data.size() + n > max_size()) return nullptr;
	auto buf_end = _buffer.get() + _capacity;
	if (static_cast<std::size_t>(std::distance(_data.end(), buf_end)) < n)
		[[unlikely]] {
		_capacity =
			std::min(std::max(_capacity * 2, _data.size() + n), max_size());
		std::unique_ptr<mutable_buffer::char_type[]> new_buf =
			std::make_unique<mutable_buffer::char_type[]>(_capacity);
		_data = mutable_buffer(
			new_buf.get(),
			std::copy(_data.begin(), _data.end(), new_buf.get()));
		buf_end = _buffer.get() + _capacity;
	}
	return mutable_buffer(_data.end(), buf_end);
}

void dynamic_buffer_write::push(std::size_t n) noexcept {
	_data = mutable_buffer(_data.begin(), _data.end() + n);
}

bool dynamic_buffer_write::push(const_buffer b) noexcept {
	mutable_buffer pblock = get_push_block(b.size());
	if (pblock == nullptr) return false;
	std::copy(b.begin(), b.end(), pblock.begin());
	push(b.size());
	return true;
}

dynamic_buffer::dynamic_buffer(
	std::size_t max_size, std::size_t initial_capacity)
	: dynamic_buffer_core(max_size, initial_capacity) {}

} // namespace polus