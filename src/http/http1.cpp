#include <iterator>
#include <polus/http/http1.hpp>
#include <polus/log.hpp>
#include <polus/utils/static_buffer.hpp>
#include <polus/utils/views.hpp>
#include <ranges>

namespace polus {

http1_buf::http1_buf(socket_buf &socket, bool server_side)
	: _socket(&socket), _server_side(server_side), _wstatus(write_status::top),
	  _closed_write_side(false), _whas_body(false), _wchunked(false),
	  _wtrailer(false), _rstatus(read_status::top), _rhas_body(false),
	  _rchunked(false), _rtrailer(false) {
	setp(_wbuffer.begin(), _wbuffer.end());
	setg(_wbuffer.begin(), _wbuffer.begin(), _wbuffer.begin());
}

header http1_buf::try_read_header() { return _rheaders.try_read(); }

template <std::unsigned_integral Int> constexpr Int log2p1(Int n) {
	return n ? 1 + log2p1(static_cast<Int>(n >> 1)) : 0;
}

template <std::unsigned_integral Int>
static const_buffer make_decimal_number(Int num) {
	thread_local static_buffer<(8 * sizeof(Int)) / log2p1(10u) + 1> buffer;
	auto it = std::prev(buffer.end());
	for (; num; --it, num /= 10) *it = '0' + (num % 10);
	return const_buffer(std::next(it), buffer.end());
}

static const_buffer get_version_string(http_version v) {
	switch (v) {
	case http_version::v10: return "HTTP/1.0"_cb;
	case http_version::v11: return "HTTP/1.1"_cb;
	case http_version::v2: return "HTTP/2"_cb;
	case http_version::v3: return "HTTP/3"_cb;
	default: return "HTTP/U"_cb;
	}
}

bool http1_buf::write_end(bool b) {
	if (!b) {
		setp(nullptr, nullptr);
		_wstatus = write_status::end;
	}
	return b;
}

#define CHECK_WRITE_END(b)                       \
	{                                            \
		if (!this->write_end((b))) return false; \
	}

bool http1_buf::read_end(bool b) {
	if (!b) {
		setg(nullptr, nullptr, nullptr);
		_rstatus = read_status::end;
	}
	return b;
}

#define CHECK_READ_END(b)                       \
	{                                           \
		if (!this->read_end((b))) return false; \
	}

bool http1_buf::write_top(
	http_version v, status_code_type status, const_buffer status_text) {
	if (_wstatus != write_status::top) throw http1_past_top_phase();
	const_buffer status_str = make_decimal_number(status);
	auto it = std::ostreambuf_iterator<http1_buf::char_type>(_socket);
	copy_views(
		it, get_version_string(v), " "_cb, status_str, " "_cb, status_text,
		"\r\n"_cb);
	_wstatus = write_status::headers;
	return write_end(!it.failed());
}

bool http1_buf::write_top(
	const_buffer method, const_buffer path, http_version v) {
	if (_wstatus != write_status::top) throw http1_past_top_phase();
	auto it = std::ostreambuf_iterator<http1_buf::char_type>(_socket);
	copy_views(
		it, method, " "_cb, path, " "_cb, get_version_string(v), "\r\n"_cb);
	_wstatus = write_status::headers;
	return write_end(!it.failed());
}

bool http1_buf::write_header(header h) {
	_wheaders.add(h);
	return true;
}

void http1_buf::close_write_side() { _closed_write_side = true; }

void http1_buf::read_abort() { return _socket->read_abort(); }
void http1_buf::write_abort() { return _socket->write_abort(); }

template <typename It> static It write_header(It it, header h) {
	return copy_views(it, h.name, ": "_cb, h.value, "\r\n"_cb);
}

static std::size_t str_to_num(const_buffer str) {
	std::size_t num = 0;
	for (char c : str) num = num * 10 + (c - '0');
	return num;
};

template <bool Trailer>
bool http1_buf::write_headers(bool &done, const_buffer data) {
	std::ostreambuf_iterator io_it(_socket);
	while (true) {
		header h = _wheaders.try_read();
		if (h == nullptr) break;
		io_it = copy_views(io_it, h.name, ": "_cb, h.value, "\r\n"_cb);
		CHECK_WRITE_END(!io_it.failed())

		if constexpr (!Trailer) {
			const auto tolower = std::views::transform(buffer::tolower);
			const auto name_tolower = h.name | tolower;
			auto eq = buffer::buffer_equals();
			if (eq(name_tolower, "content-length"_cb)) {
				_whas_body = true;
				_wchunked = false;
				_wsize = str_to_num(h.value);
			} else if (eq(name_tolower, "transfer-encoding"_cb)) {
				const auto val_tolower =
					h.value | std::views::transform(buffer::tolower);
				const auto transform_found_it =
					find_delimiter(val_tolower, "chunked"_cb);
				if (transform_found_it.base() != h.value.end()) {
					_whas_body = true;
					_wchunked = true;
				}
			} else if (eq(name_tolower, "trailer"_cb)) {
				_wtrailer = true;
			}
		}
	}

	if (!data.size() && !_closed_write_side) return done = true;

	copy_views(io_it, "\r\n"_cb);
	CHECK_WRITE_END(!io_it.failed())
	if constexpr (Trailer) {
		_wstatus = write_status::end;
	} else {
		_wstatus = _whas_body ? (_wchunked ? write_status::body_chunked
										   : write_status::body_serial)
							  : write_status::end;
	}
	return true;
}

template bool http1_buf::write_headers<true>(bool &done, const_buffer data);
template bool http1_buf::write_headers<false>(bool &done, const_buffer data);

template <std::unsigned_integral Int>
static const_buffer make_hex_number(Int num) {
	constexpr char hexchars[] = "0123456789abcdef";
	thread_local static_buffer<(8 * sizeof(Int)) / 4> buffer;
	auto it = std::prev(buffer.end());
	for (; num; --it, num /= 16) *it = hexchars[num % 16];
	return const_buffer(std::next(it), buffer.end());
}

bool http1_buf::write_chunked(bool &done, const_buffer data) {
	std::ostreambuf_iterator io_it(_socket);

	if (data.size() > 0) {
		auto hexnum = make_hex_number(data.size());
		copy_views(io_it, hexnum, "\r\n"_cb);
		CHECK_WRITE_END(!io_it.failed())
		auto written = _socket->sputn(data.begin(), data.size());
		CHECK_WRITE_END(static_cast<std::size_t>(written) == data.size())
		copy_views(io_it, "\r\n"_cb);
		CHECK_WRITE_END(!io_it.failed())
	}
	setp(_wbuffer.begin(), _wbuffer.end());

	if (!_closed_write_side) return done = true;

	copy_views(io_it, "0\r\n\r\n"_cb);
	CHECK_WRITE_END(!io_it.failed())
	_wstatus = _wtrailer ? write_status::trailer : write_status::end;
	return true;
}

bool http1_buf::write_serial(bool &done, const_buffer data) {
	std::size_t write_size = std::min(data.size(), _wsize);
	auto written = _socket->sputn(data.begin(), write_size);
	CHECK_WRITE_END(static_cast<std::size_t>(written) == write_size)
	_wsize -= write_size;
	setp(
		_wbuffer.begin(),
		std::min(std::next(_wbuffer.begin(), _wsize), _wbuffer.end()));

	if (!_closed_write_side && _wsize) {
		done = true;
	} else {
		_wstatus = write_status::end;
	}
	return true;
}

bool http1_buf::__sync() {
	if (_wstatus == write_status::end) return false;

	const_buffer data(pbase(), pptr());
	for (bool done = false; !done;) {
		switch (_wstatus) {
		case write_status::top: throw http1_did_not_send_top();
		case write_status::headers:
			if (!write_headers<false>(done, data)) return false;
			break;
		case write_status::body_chunked:
			if (!write_chunked(done, data)) return false;
			break;
		case write_status::body_serial:
			if (!write_serial(done, data)) return false;
			break;
		case write_status::trailer:
			if (!write_headers<true>(done, data)) return false;
			break;
		case write_status::end: done = true;
		}
	}

	return true;
}

template <class CharT, class Traits>
static auto istreambuf_range(std::basic_streambuf<CharT, Traits> *buf) {
	return std::ranges::subrange(
		std::istreambuf_iterator(buf), std::default_sentinel);
}

bool http1_buf::read_line(const_buffer &line) {
	auto [bufend, outend] =
		find_delimiter_and_copy(istreambuf_range(_socket), "\r\n"_cb, _rbuffer);
	if (outend == _rbuffer.end()) {
		throw http1_line_too_long();
	} else if (bufend == std::default_sentinel) {
		return false;
	}
	line = const_buffer(_rbuffer.begin(), outend);
	return true;
}

bool http1_buf::read_top() {
	const_buffer line;
	if (!read_line(line)) return false;

	const_buffer a(_server_side ? ":method"_cb : ":version"_cb),
		b(_server_side ? ":path"_cb : ":status"_cb),
		c(_server_side ? ":version"_cb : ":status_text"_cb);

	auto found = line.find(' ');
	_rheaders.add(a, const_buffer(line.begin(), found));
	line = line.shift(found + 1);
	found = line.find(' ');
	_rheaders.add(b, const_buffer(line.begin(), found));
	line = line.shift(found + 1);
	_rheaders.add(c, const_buffer(line.begin(), std::prev(line.end(), 2)));
	_rstatus = read_status::headers;
	return true;
}

bool http1_buf::read_header() {
	const_buffer line;
	if (!read_line(line)) [[unlikely]]
		return false;
	auto hstr = line.shrink(std::prev(line.end(), 2));
	if (!hstr.size()) {
		if (_rhas_body) {
			_rstatus = _rchunked ? read_status::body_chunk_size
								 : read_status::body_serial;
		} else {
			_rstatus = read_status::end;
		}
		return true;
	}
	header h = header::from_line(hstr);

	const auto tolower = std::views::transform(buffer::tolower);
	const auto name_tolower = h.name | tolower;
	auto eq = buffer::buffer_equals();
	if (eq(name_tolower, "content-length"_cb)) {
		_rhas_body = true;
		_rchunked = false;
		_rnext_size = str_to_num(h.value);
	} else if (eq(name_tolower, "transfer-encoding"_cb)) {
		const auto val_tolower =
			h.value | std::views::transform(buffer::tolower);
		const auto transform_found_it =
			find_delimiter(val_tolower, "chunked"_cb);
		if (transform_found_it.base() != h.value.end()) {
			_rhas_body = true;
			_rchunked = true;
		}
	} else if (eq(name_tolower, "trailer"_cb)) {
		_rtrailer = true;
	}

	_rheaders.add(h);
	return true;
}

bool http1_buf::read_serial(bool &done) {
	if (_socket->sgetc() == socket_buf::traits_type::eof()) [[unlikely]]
		return read_end(false);
	std::size_t read_size = std::min(
		_rbuffer.size(),
		std::min<std::size_t>(_socket->in_avail(), _rnext_size));
	auto readres = _socket->sgetn(_rbuffer.begin(), read_size);
	if (readres < 0) throw polus_exception();
	if (static_cast<std::size_t>(readres) != read_size) [[unlikely]]
		return read_end(false);
	_rnext_size -= read_size;
	setg(
		_rbuffer.begin(), _rbuffer.begin(),
		std::next(_rbuffer.begin(), read_size));
	done = true;
	if (!_rnext_size) _rstatus = read_status::check_trailer;
	return true;
}

static constexpr std::size_t hexstr_to_num(const_buffer str) {
	constexpr std::size_t hex_values[256] = {
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  1,  2,  3, 4, 5, 6, 7, 8, 9,	0,	0,
		0,	0,	0,	0, 0, 10, 11, 12, 13, 14, 15, 0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 10, 11, 12,
		13, 14, 15, 0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0, 0, 0,	0,	0,
		0,	0,	0,	0, 0, 0,  0,  0,  0,  0,  0,  0, 0, 0, 0, 0,
	};
	std::size_t num = 0;
	for (char c : str)
		num = num * 0x10 + hex_values[static_cast<std::uint16_t>(c)];
	return num;
};

bool http1_buf::read_chunk_size() {
	const_buffer line;
	if (!read_line(line)) [[unlikely]]
		return false;
	line = line.shrink(std::prev(line.end(), 2));
	line = line.shrink(line.find(';'));
	_rnext_size = hexstr_to_num(line);
	_rstatus = _rnext_size ? read_status::body_chunked
						   : read_status::body_last_chunk_crlf;
	return true;
}

bool http1_buf::read_chunk(bool &done) {
	if (_socket->sgetc() == socket_buf::traits_type::eof()) [[unlikely]]
		return read_end(false);
	std::size_t read_size = std::min(
		_rbuffer.size(),
		std::min<std::size_t>(_socket->in_avail(), _rnext_size));
	auto readres = _socket->sgetn(_rbuffer.begin(), read_size);
	if (static_cast<std::size_t>(readres) != read_size) [[unlikely]]
		return read_end(false);
	_rnext_size -= read_size;
	setg(
		_rbuffer.begin(), _rbuffer.begin(),
		std::next(_rbuffer.begin(), read_size));
	done = true;
	if (!_rnext_size) _rstatus = read_status::body_chunk_end_crlf;
	return true;
}

bool http1_buf::read_chunk_crlf() {
	if (!traits_type::not_eof(_socket->snextc()) ||
		!traits_type::not_eof(_socket->snextc()))
		return read_end(false);
	_rstatus = read_status::body_chunk_size;
	return true;
}

bool http1_buf::read_last_chunk_crlf() {
	if (!traits_type::not_eof(_socket->sbumpc()) ||
		!traits_type::not_eof(_socket->sbumpc()))
		return read_end(false);
	_rstatus = read_status::check_trailer;
	return true;
}

void http1_buf::check_trailer() {
	_rstatus = _rtrailer ? read_status::trailer : read_status::end;
}

bool http1_buf::read_trailer() {
	const_buffer line;
	if (!read_line(line)) [[unlikely]]
		return false;
	auto hstr = line.shrink(std::prev(line.end(), 2));
	if (!hstr.size()) {
		_rstatus = read_status::end;
	} else {
		header h = header::from_line(hstr);
		_rheaders.add(h);
	}
	return true;
}

bool http1_buf::__rsync() {
	if (_rstatus == read_status::end) return false;

	bool done = false;
	while (!done) {
		switch (_rstatus) {
		case read_status::top:
			if (!read_top()) return false;
			break;
		case read_status::headers:
			if (!read_header()) return false;
			break;
		case read_status::body_serial:
			if (!read_serial(done)) return false;
			break;
		case read_status::body_chunk_size:
			if (!read_chunk_size()) return false;
			break;
		case read_status::body_chunked:
			if (!read_chunk(done)) return false;
			break;
		case read_status::body_chunk_end_crlf:
			if (!read_chunk_crlf()) return false;
			break;
		case read_status::body_last_chunk_crlf:
			if (!read_last_chunk_crlf()) return false;
			break;
		case read_status::check_trailer: check_trailer(); break;
		case read_status::trailer:
			if (!read_trailer()) return false;
			break;
		case read_status::end: return false;
		default: break;
		}
	}

	return true;
}

http1_buf::int_type http1_buf::overflow(int_type ch) {
	if (pptr() != epptr()) {
		if (traits_type::eq_int_type(ch, traits_type::eof()))
			return traits_type::to_int_type(0);
		*pptr() = traits_type::to_char_type(ch);
		pbump(1);
		return ch;
	}
	if (!__sync()) return traits_type::eof();
	if (traits_type::eq_int_type(ch, traits_type::eof()))
		return traits_type::to_int_type(0);
	*pptr() = traits_type::to_char_type(ch);
	pbump(1);
	return ch;
}

http1_buf::int_type http1_buf::underflow() {
	return gptr() >= egptr() && __rsync() ? traits_type::to_int_type(*gptr())
										  : traits_type::eof();
}

int http1_buf::sync() {
	if (!__sync()) return 1;
	return _socket->pubsync();
}

bool http1_buf::read_chunk_to(mutable_buffer &buf) {
	std::size_t read_size = std::min<std::size_t>(_rnext_size, buf.size());
	auto readres = _socket->sgetn(buf.begin(), read_size);
	buf = buf.shift(readres);
	if (static_cast<std::size_t>(readres) != read_size) return false;
	_rnext_size -= read_size;
	if (!_rnext_size) _rstatus = read_status::body_chunk_end_crlf;
	return true;
}

bool http1_buf::read_serial_to(mutable_buffer &buf) {
	std::size_t read_size = std::min<std::size_t>(_rnext_size, buf.size());
	auto readres = _socket->sgetn(buf.begin(), read_size);
	buf = buf.shift(readres);
	if (static_cast<std::size_t>(readres) != read_size) return false;
	_rnext_size -= read_size;
	if (!_rnext_size) _rstatus = read_status::check_trailer;
	return true;
}

std::streamsize http1_buf::xsgetn(char_type *s, std::streamsize count) {
	if (_rstatus == read_status::end) return 0;

	mutable_buffer out(s, s + count);
	mutable_buffer buffered(gptr(), egptr());
	buffered = buffered.shrink(std::min<std::size_t>(buffered.size(), count));
	out = out << buffered;
	gbump(buffered.size());

	while (out.size()) {
		switch (_rstatus) {
		case read_status::top:
			if (!read_top()) return out.begin() - s;
			break;
		case read_status::headers:
			if (!read_header()) return out.begin() - s;
			break;
		case read_status::body_serial:
			if (!read_serial_to(out)) return out.begin() - s;
			break;
		case read_status::body_chunk_size:
			if (!read_chunk_size()) return out.begin() - s;
			break;
		case read_status::body_chunked:
			if (read_chunk_to(out)) return out.begin() - s;
			break;
		case read_status::body_chunk_end_crlf:
			if (read_chunk_crlf()) return out.begin() - s;
			break;
		case read_status::body_last_chunk_crlf:
			if (read_last_chunk_crlf()) return out.begin() - s;
			break;
		case read_status::check_trailer: check_trailer(); break;
		case read_status::trailer:
			if (!read_trailer()) return out.begin() - s;
			break;
		case read_status::end: return out.begin() - s;
		default: break;
		}
	}

	return count;
}

std::streamsize http1_buf::xsputn(const char_type *s, std::streamsize count) {
	if (_wstatus == write_status::end) return false;

	if (!__sync()) return 0;

	const_buffer data(s, s + count);
	for (bool done = false; !done;) {
		switch (_wstatus) {
		case write_status::top: throw http1_did_not_send_top();
		case write_status::headers:
			if (!write_headers<false>(done, data)) return 0;
			break;
		case write_status::body_chunked:
			if (!write_chunked(done, data)) return 0;
			break;
		case write_status::body_serial:
			if (!write_serial(done, data)) return 0;
			break;
		case write_status::trailer:
			if (!write_headers<true>(done, data)) return count;
			break;
		case write_status::end: return count;
		}
	}

	return count;
}

} // namespace polus
