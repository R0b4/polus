#include <polus/http/http.hpp>

namespace polus {

static constexpr const_buffer trim_whitespace(const_buffer b) {
	auto begin = b.begin();
	for (; begin < b.end() && (*begin == ' ' || *begin == '\t'); ++begin)
		;
	auto end = std::prev(b.end());
	for (; (*end == ' ' || *end == '\t') && end >= begin; --end)
		;
	if (begin > end) return nullptr;
	return const_buffer(begin, std::next(end));
}

header header::from_line(const_buffer line) {
	auto it = line.find(':');
	auto name = const_buffer(line.begin(), it);
	auto value = const_buffer(std::next(it), line.end());
	return header(name, trim_whitespace(value));
}

} // namespace polus