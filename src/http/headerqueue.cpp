#include <polus/http/headerqueue.hpp>

namespace polus {

header_queue::header_queue(std::size_t maximum_headers_length)
	: _buf(maximum_headers_length) {}

bool header_queue::add(header h) {
	return _buf.push(h.name) && _buf.push(":") && _buf.push(h.value) &&
		   _buf.push("\n");
}

header header_queue::try_read() {
	auto data = _buf.data();
	auto endline = data.find('\n');
	if (endline == data.end()) return header::err();
	data = data.shrink(endline);
	_buf.consume(data.size() + 1);
	return header::from_line(data);
}

} // namespace polus