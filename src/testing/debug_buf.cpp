#include <polus/fiber/condition_variable.hpp>
#include <polus/testing/debug_buf.hpp>

namespace polus::testing {

struct __internal_shared_buf_state_part {
	fib::mutex _mut;
	bool _end_conn = false;
	bool _read_abort = false;
	bool _write_into_abort = false;
	fib::condition_variable _var;
	const_buffer write_to_buffer;
	std::optional<std::size_t> write_result;

	std::size_t read(mutable_buffer);
	std::size_t write_to(const_buffer);

	std::size_t read_until(mutable_buffer, time_point_type);
	std::size_t write_to_until(const_buffer, time_point_type);
};

struct __internal_shared_buf_state {
	std::array<__internal_shared_buf_state_part, 2> _states;
};

std::size_t __internal_shared_buf_state_part::read(mutable_buffer buf) {
	std::unique_lock _lock(_mut);
	_var.wait(_lock, [this] {
		return write_to_buffer != nullptr || _end_conn || _read_abort;
	});
	if (write_to_buffer != nullptr) {
		write_to_buffer = write_to_buffer.shrink(
			std::min(write_to_buffer.size(), buf.size()));
		buf << write_to_buffer;
		write_result = write_to_buffer.size();
		write_to_buffer = nullptr;
		_var.notify_one();
		return *write_result;
	}
	if (_end_conn) return 0;
	throw socket_aborted_error();
}

std::size_t __internal_shared_buf_state_part::write_to(const_buffer buf) {
	std::unique_lock _lock(_mut);
	write_to_buffer = buf;
	_var.notify_one();
	_var.wait(_lock, [this] {
		return write_result.has_value() || _end_conn || _write_into_abort;
	});
	if (write_result.has_value()) {
		auto res = *write_result;
		write_result.reset();
		return res;
	}
	if (_end_conn) return 0;
	throw socket_aborted_error();
}

std::size_t __internal_shared_buf_state_part::read_until(
	mutable_buffer buf, time_point_type time) {
	std::unique_lock _lock(_mut);
	_var.wait_until(_lock, time, [this] {
		return write_to_buffer != nullptr || _end_conn || _read_abort;
	});
	if (write_to_buffer != nullptr) {
		write_to_buffer = write_to_buffer.shrink(
			std::min(write_to_buffer.size(), buf.size()));
		buf << write_to_buffer;
		write_result = write_to_buffer.size();
		write_to_buffer = nullptr;
		_var.notify_one();
		return *write_result;
	}
	if (_end_conn) return 0;
	if (_read_abort) throw socket_aborted_error();
	throw socket_timeout_error();
}

std::size_t __internal_shared_buf_state_part::write_to_until(
	const_buffer buf, time_point_type time) {
	std::unique_lock _lock(_mut);
	write_to_buffer = buf;
	_var.notify_one();
	_var.wait_until(_lock, time, [this] {
		return write_result.has_value() || _end_conn || _write_into_abort;
	});
	if (write_result.has_value()) {
		write_result.reset();
		return *write_result;
	}
	if (_end_conn) return 0;
	if (_write_into_abort) throw socket_aborted_error();
	throw socket_timeout_error();
}

internal_shared_buf::internal_shared_buf(
	int bufidx, std::shared_ptr<__internal_shared_buf_state> state)
	: _bufidx(bufidx), _state(state){};

void internal_shared_buf::read_abort() {
	std::lock_guard _guard(my_state()._mut);
	my_state()._read_abort = true;
	my_state()._var.notify_one();
}

void internal_shared_buf::write_abort() {
	std::lock_guard _guard(other_state()._mut);
	other_state()._write_into_abort = true;
	other_state()._var.notify_one();
}

std::size_t internal_shared_buf::read(mutable_buffer buf) {
	return my_state().read(buf);
}

std::size_t
internal_shared_buf::read_until(mutable_buffer buf, time_point_type time) {
	return my_state().read_until(buf, time);
}

std::size_t internal_shared_buf::write(const_buffer buf) {
	return other_state().write_to(buf);
}

std::size_t
internal_shared_buf::write_until(const_buffer buf, time_point_type time) {
	return other_state().write_to_until(buf, time);
}

internal_shared_buf::~internal_shared_buf() {
	{
		std::lock_guard _lock(my_state()._mut);
		my_state()._end_conn = true;
		my_state()._var.notify_one();
	}
	{
		std::lock_guard _lock(other_state()._mut);
		other_state()._end_conn = true;
		other_state()._var.notify_one();
	}
}

__internal_shared_buf_state_part &internal_shared_buf::my_state() {
	return _state->_states[_bufidx];
}

__internal_shared_buf_state_part &internal_shared_buf::other_state() {
	return _state->_states[1 - _bufidx];
}

std::pair<
	std::unique_ptr<internal_shared_buf>, std::unique_ptr<internal_shared_buf>>
make_shared_buf_pair() {
	auto state = std::make_shared<__internal_shared_buf_state>();
	return std::make_pair(
		std::make_unique<internal_shared_buf>(0, state),
		std::make_unique<internal_shared_buf>(1, state));
}

} // namespace polus::testing