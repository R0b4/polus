#include <polus/os/os.hpp>

#ifdef POLUS_UNIX

#include <algorithm>
#include <iostream>
#include <memory>
#include <mutex>
#include <ranges>

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <polus/log.hpp>
#include <polus/os/file.hpp>
#include <polus/utils/exception.hpp>
#include <polus/utils/static_buffer.hpp>

#ifdef POLUS_LINUX

#include <sys/epoll.h>
#include <time.h>

#else

#include <queue>
#include <unordered_map>

#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#endif

namespace polus::os {

struct file {
	int fd;
};

file *open_tcp_listener(const char *port) {
	// source:
	// https://beej.us/guide/bgnet/
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	int yes = 1;
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	for (;;) {
		if ((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
			if (rv == EAGAIN) {
				continue;
			} else {
				throw std::runtime_error("Failed to get "
										 "address info");
			}
		}
		break;
	}

	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) ==
			-1) {
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) ==
			-1) {
			::close(sockfd);
			freeaddrinfo(servinfo);
			throw_errno_err();
		}

		if (::bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			::close(sockfd);
			continue;
		}

		break;
	}

	freeaddrinfo(servinfo);
	if (p == NULL) { throw std::runtime_error("Socket failed to bind"); }
	if (fcntl(sockfd, F_SETFL, O_NONBLOCK) == -1) { throw_errno_err(); }
	if (::listen(sockfd, 100) == -1) { throw_errno_err(); }

	return new file{sockfd};
}

file *tcp_connect(
	[[maybe_unused]] const char *peer, [[maybe_unused]] const char *service) {
	return nullptr;
}

os::file *accept(os::file *listener) {
	for (;;) {
		sockaddr_storage their_addr;
		socklen_t sinsize = sizeof(their_addr);
		int fd = ::accept4(
			listener->fd, (sockaddr *)&their_addr, &sinsize, SOCK_NONBLOCK);
		if (fd != -1) return new file{fd};
		auto err = errno;
		if (err == EAGAIN || err == EWOULDBLOCK) return nullptr;
		if (err == ECONNABORTED || err == EINTR) continue;
		throw_errno_err();
	}
}

std::pair<file *, file *> make_pipe() {
	int fds[2];
#ifdef POLUS_LINUX
	if (pipe2(fds, O_NONBLOCK) == -1) throw_errno_err();
#elif defined(POLUS_UNIX)
	if (pipe(fds) == -1) throw_errno_err();
	if (fcntl(fds[0], F_SETFL, O_NONBLOCK) == -1) throw_errno_err();
	if (fcntl(fds[1], F_SETFL, O_NONBLOCK) == -1) throw_errno_err();
#endif
	return std::make_pair(new file{fds[0]}, new file{fds[1]});
}

std::size_t read(file *file, mutable_buffer buf) {
	for (;;) {
		::ssize_t ret = ::read(file->fd, buf.begin(), buf.size());
		if (ret == 0) return file_closed;
		if (ret != -1) return ret;
		auto err = errno;
		if (err == EAGAIN || err == EWOULDBLOCK) {
			return would_block;
		} else if (err == ECONNRESET || err == EPIPE) {
			return file_closed;
		} else if (err == EINTR) {
			continue;
		} else {
			throw_errno_err();
		}
	}
}

int startup_disable_sigpipe() {
	signal(SIGPIPE, SIG_IGN);
	return 0;
}
static int _startup_ignore = startup_disable_sigpipe();

std::size_t write(file *file, const_buffer buf) {
	for (;;) {
		::ssize_t ret = ::write(file->fd, buf.begin(), buf.size());
		if (ret == 0) return file_closed;
		if (ret != -1) return ret;
		auto err = errno;
		if (err == EAGAIN || err == EWOULDBLOCK) {
			return would_block;
		} else if (err == ECONNRESET || err == EPIPE) {
			return file_closed;
		} else if (err == EINTR) {
			continue;
		} else {
			throw_errno_err();
		}
	}
}

void close(file *file) {
	::close(file->fd);
	delete file;
}

#ifdef POLUS_LINUX

struct poll {
	poll(int epollfd, std::size_t begin_size)
		: epollfd(epollfd), events(begin_size) {
		it_begin = events.begin();
		it_end = events.begin();
	}
	int epollfd;
	std::vector<epoll_event> events;
	std::vector<epoll_event>::iterator it_begin;
	std::vector<epoll_event>::iterator it_end;
};

poll *make_poll() {
	int efd = epoll_create1(0);
	if (efd == -1) throw_errno_err(efd);
	return new poll(efd, 100);
}

static auto throw_errno_err_on_m1(auto b) {
	if (b == -1) [[unlikely]]
		throw_errno_err();
	return b;
}

void poll_add(poll *p, file *f, poll_id_type id) {
	epoll_event ev{.events = EPOLLOUT | EPOLLIN | EPOLLET, .data{.u64 = id}};
	throw_errno_err_on_m1(epoll_ctl(p->epollfd, EPOLL_CTL_ADD, f->fd, &ev));
}

void poll_remove(poll *p, file *f, [[maybe_unused]] poll_id_type id) {
	throw_errno_err_on_m1(epoll_ctl(p->epollfd, EPOLL_CTL_DEL, f->fd, nullptr));
}

static events_mask convert_epoll_evs(decltype(epoll_event::events) evs) {
	auto help = [evs](auto x, events_mask y) {
		return evs & x ? y : no_events;
	};
	return help(EPOLLOUT, event_write) | help(EPOLLIN, event_read) |
		   help(EPOLLHUP, event_close) | help(EPOLLERR, event_err);
}

std::pair<poll_id_type, events_mask> poll_get_event(poll *p) {
	auto it = std::find_if(p->it_begin, p->it_end, [](epoll_event &e) {
		return e.data.u64 != poll_id_type::invalid();
	});
	if (it == p->it_end)
		return std::make_pair(poll_id_type::invalid(), no_events);
	++p->it_begin;
	return std::make_pair(
		static_cast<poll_id_type>(it->data.u64), convert_epoll_evs(it->events));
}

void poll_wait_until(poll *p, time_point_type time) {
	if (p->it_begin != p->it_end) return;
	if (p->it_end == p->events.end())
		p->events.resize(p->events.size() * 2 + 1);
	for (auto now = clock_type::now(); now < time; now = clock_type::now()) {
		auto duration = time - now;
		auto milli =
			std::chrono::duration_cast<std::chrono::milliseconds>(duration);
		int count = epoll_wait(
			p->epollfd, &*p->events.begin(), p->events.size(), milli.count());
		if (count == -1) {
			switch (errno) {
			case EINTR: continue;
			default: throw_errno_err();
			}
		}
		p->it_begin = p->events.begin();
		p->it_end = std::next(p->it_begin, count);
		return;
	}
}

void poll_report_ioblock(poll *, poll_id_type, events_mask) {}

void poll_wait(poll *p) {
	if (p->it_begin != p->it_end) return;
	if (p->it_end == p->events.end())
		p->events.resize(p->events.size() * 2 + 1);
	for (;;) {
		int count =
			epoll_wait(p->epollfd, &*p->events.begin(), p->events.size(), -1);
		if (count == -1) {
			switch (errno) {
			case EINTR: continue;
			default: throw_errno_err();
			}
		}
		p->it_begin = p->events.begin();
		p->it_end = std::next(p->it_begin, count);
		return;
	}
}

void close_poll(poll *p) {
	::close(p->epollfd);
	delete p;
}

#else

struct poll {
	std::mutex _mut;
	std::vector<::pollfd> pollfds;
	std::vector<poll_id_type> poll_ids;
	std::size_t used_pollfds_size = 0;
	std::unordered_map<poll_id_type, std::size_t> id_to_index;

	std::queue<std::pair<poll_id_type, events_mask>> result;

	std::condition_variable exec_var;
	std::mutex _exec_mut;
	std::size_t userland_counter = 0;
	bool running_syscall = false;
	int cancel_pipes[2];
};

template <std::invocable Func> struct destructor_lambda {
	destructor_lambda(Func &&func) : _func(std::forward<Func>(func)) {}
	~destructor_lambda() { _func(); }
	Func _func;
};

void poll_done_usertime(poll *p);
auto poll_wait_usertime(poll *p) {
	std::unique_lock _lock(p->_exec_mut);
	++p->userland_counter;
	auto allow_userlandswitch = [p] { return !p->running_syscall; };
	if (!allow_userlandswitch()) {
		int written = ::write(p->cancel_pipes[1], " ", 1);
		if (written == -1) throw_errno_err();
	}
	p->exec_var.wait(_lock, allow_userlandswitch);
	return destructor_lambda(polus::bind(poll_done_usertime, p));
}

void poll_done_usertime(poll *p) {
	std::unique_lock _lock(p->_exec_mut);
	--p->userland_counter;
	if (p->userland_counter == 0) p->exec_var.notify_all();
}

void poll_done_kerneltime(poll *p);
auto poll_wait_kerneltime(poll *p) {
	std::unique_lock _lock(p->_exec_mut);
	p->exec_var.wait(_lock, [p] { return p->userland_counter == 0; });
	p->running_syscall = true;
	return destructor_lambda(polus::bind(poll_done_kerneltime, p));
}

void poll_done_kerneltime(poll *p) {
	std::unique_lock _lock(p->_exec_mut);
	p->running_syscall = false;
	p->exec_var.notify_all();
}

static events_mask convert_poll_evs(decltype(::pollfd::events) evs) {
	auto help = [evs](decltype(::pollfd::events) x, events_mask y) {
		return evs & x ? y : no_events;
	};
	return help(POLLOUT, event_write) | help(POLLIN, event_read) |
		   help(POLLHUP, event_close) | help(POLLERR, event_err);
}

static decltype(::pollfd::events) convert_evs_to_poll(events_mask evs) {
	auto help = [evs](decltype(::pollfd::events) y, events_mask x) {
		return evs & x ? y : no_events;
	};
	return help(POLLOUT, event_write) | help(POLLIN, event_read) |
		   help(POLLHUP, event_close) | help(POLLERR, event_err);
}

poll *make_poll() {
	auto *p = new poll;
	if (pipe(p->cancel_pipes) == -1) throw_errno_err();
	if (fcntl(p->cancel_pipes[1], F_SETFL, O_NONBLOCK) == -1) throw_errno_err();
	if (fcntl(p->cancel_pipes[0], F_SETFL, O_NONBLOCK) == -1) throw_errno_err();
	++p->used_pollfds_size;
	::pollfd pfd;
	pfd.fd = p->cancel_pipes[0];
	pfd.events = POLLIN;
	p->pollfds.push_back(pfd);
	p->poll_ids.push_back(poll_id_type::invalid());
	return p;
}

static std::pair<
	std::vector<::pollfd>::iterator, std::vector<poll_id_type>::iterator>
make_new_pfd(poll *p) {
	if (p->used_pollfds_size == p->pollfds.size()) {
		p->pollfds.emplace_back();
		p->poll_ids.emplace_back();
		++p->used_pollfds_size;
		return {std::prev(p->pollfds.end()), std::prev(p->poll_ids.end())};
	} else {
		auto idx = p->used_pollfds_size++;
		return {
			std::next(p->pollfds.begin(), idx),
			std::next(p->poll_ids.begin(), idx)};
	}
}

void poll_add(poll *p, file *f, poll_id_type id) {
	auto usrlock = poll_wait_usertime(p);
	std::lock_guard _lock(p->_mut);
	auto [pfd, pid] = make_new_pfd(p);
	pfd->fd = f->fd;
	*pid = id;
	p->id_to_index[id] = std::distance(p->pollfds.begin(), pfd);
}

void poll_report_ioblock(poll *p, poll_id_type id, events_mask evs) {
	auto usrlock = poll_wait_usertime(p);
	std::lock_guard _lock(p->_mut);
	::pollfd &pfd = p->pollfds[p->id_to_index[id]];
	pfd.events |= convert_evs_to_poll(evs);
}

void poll_remove(poll *p, [[maybe_unused]] file *f, poll_id_type id) {
	auto usrlock = poll_wait_usertime(p);
	std::lock_guard _lock(p->_mut);
	std::size_t index = p->id_to_index[id];
	p->id_to_index.erase(id);
	--p->used_pollfds_size;
	if (index == p->used_pollfds_size) return;
	std::swap(p->pollfds[index], p->pollfds[p->used_pollfds_size]);
	std::swap(p->poll_ids[index], p->poll_ids[p->used_pollfds_size]);
	p->id_to_index[p->poll_ids[index]] = index;
}

std::pair<poll_id_type, events_mask> poll_get_event(poll *p) {
	auto usrlock = poll_wait_usertime(p);
	if (p->result.empty())
		return std::make_pair(poll_id_type::invalid(), no_events);
	auto r = p->result.front();
	p->result.pop();
	return r;
}

static void poll_clear_cancelpipe(poll *p) {
	while (true) {
		char out;
		auto rres = ::read(p->cancel_pipes[0], &out, 1);
		if (rres == -1) {
			if (errno == EWOULDBLOCK || errno == EAGAIN) {
				break;
			} else {
				throw_errno_err();
			}
		}
	}
}

void poll_wait_until(poll *p, time_point_type time) {
	for (auto now = clock_type::now(); now < time && p->result.empty();
		 now = clock_type::now()) {
		auto duration = time - now;
		auto milli =
			std::chrono::duration_cast<std::chrono::milliseconds>(duration);
		auto syscall_lock = poll_wait_kerneltime(p);
		int count = ::poll(p->pollfds.data(), p->used_pollfds_size, -1);
		if (count == -1) {
			switch (errno) {
			case EINTR: continue;
			default: throw_errno_err();
			}
		}

		if (p->pollfds[0].revents & POLLIN) {
			poll_clear_cancelpipe(p);
			--count;
		}

		for (std::size_t i = 0; i < p->used_pollfds_size && count; ++i) {
			if (!p->pollfds[i].revents) continue;
			p->result.emplace(
				p->poll_ids[i], convert_poll_evs(p->pollfds[i].revents));
			--count;
		}
	}
}

void poll_wait(poll *p) {
	for (; p->result.empty();) {
		auto syscall_lock = poll_wait_kerneltime(p);
		int count = ::poll(p->pollfds.data(), p->used_pollfds_size, -1);
		if (count == -1) {
			switch (errno) {
			case EINTR: continue;
			default: throw_errno_err();
			}
		}

		if (p->pollfds[0].revents & POLLIN) {
			poll_clear_cancelpipe(p);
			--count;
		}

		for (std::size_t i = 1; i < p->used_pollfds_size && count; ++i) {
			if (!p->pollfds[i].revents) continue;
			p->result.emplace(
				p->poll_ids[i], convert_poll_evs(p->pollfds[i].revents));
			p->pollfds[i].events = 0;
			--count;
		}
	}
}

void close_poll(poll *p) {
	::close(p->cancel_pipes[0]);
	::close(p->cancel_pipes[1]);
	delete p;
}

#endif

} // namespace polus::os

#endif