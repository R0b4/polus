#include <polus/io/better_streambuf.hpp>

namespace polus {

mutable_buffer better_streambuf::put_area() {
	return mutable_buffer(pptr(), epptr());
}

mutable_buffer better_streambuf::put_area(mutable_buffer::iterator putptr) {
	if (putptr < pptr() || epptr() < putptr) return put_area();
	pbump(static_cast<int>(putptr - pptr()));
	if (pptr() != epptr()) return put_area();
	return traits_type::not_eof(overflow()) ? put_area() : nullptr;
}

const_buffer better_streambuf::get_area() {
	return const_buffer(gptr(), egptr());
}

const_buffer better_streambuf::get_area(const_buffer::iterator getptr) {
	if (getptr < gptr() || egptr() < getptr) return get_area();
	gbump(static_cast<int>(getptr - gptr()));
	if (gptr() != egptr()) return get_area();
	return traits_type::not_eof(underflow()) ? get_area() : nullptr;
}

} // namespace polus