#include <polus/fiber/events.hpp>
#include <polus/io/file_buf.hpp>

namespace polus {

file_buf::file_buf(os::file *f)
	: _file(f), _id(fib::register_file(f)),
	  _evs(os::event_read | os::event_write), read_aborted(false),
	  write_aborted(false) {}

void file_buf::read_abort() {
	{
		std::lock_guard __lock(_evs_mut);
		read_aborted = true;
	}
	fib::abort_wait(_id);
}

void file_buf::write_abort() {
	{
		std::lock_guard __lock(_evs_mut);
		write_aborted = true;
	}
	fib::abort_wait(_id);
}

std::size_t file_buf::read(mutable_buffer buf) {
	for (std::unique_lock lock(_evs_mut);;) {
		for (;;) {
			if (_evs & os::event_abort) {
				if (std::exchange(read_aborted, false)) {
					throw socket_aborted_error();
				} else if (!write_aborted) {
					_evs &= ~os::event_abort;
				}
			}
			if (_evs & os::event_read) break;
			if (_evs & os::event_close) return 0;
			if (_evs & os::event_err) throw filebuf_io_error();
			_evs |= fib::events_wait(_id, lock);
		}

		auto res = os::read(_file, buf);
		switch (res) {
		case os::file_closed: return 0;
		case os::would_block:
			_evs &= ~os::event_read;
			fib::events_report_ioblock(_id, os::event_read);
			break;
		default: return res;
		}
	}
}

std::size_t file_buf::read_until(mutable_buffer buf, time_point_type time) {
	for (std::unique_lock lock(_evs_mut);;) {
		for (;;) {
			if (clock_type::now() > time) throw socket_timeout_error();
			if (_evs & os::event_abort) {
				if (std::exchange(read_aborted, false)) {
					throw socket_aborted_error();
				} else if (!write_aborted) {
					_evs &= ~os::event_abort;
				}
			}
			if (_evs & os::event_read) break;
			if (_evs & os::event_close) return 0;
			if (_evs & os::event_err) throw filebuf_io_error();
			_evs |= fib::events_wait_until(_id, time, lock);
		}

		auto res = os::read(_file, buf);
		switch (res) {
		case os::file_closed: return 0;
		case os::would_block:
			_evs &= ~os::event_read;
			fib::events_report_ioblock(_id, os::event_read);
			break;
		default: return res;
		}
	}
}

std::size_t file_buf::write(const_buffer buf) {
	for (std::unique_lock lock(_evs_mut);;) {
		for (;;) {
			if (_evs & os::event_abort) {
				if (std::exchange(write_aborted, false)) {
					throw socket_aborted_error();
				} else if (!read_aborted) {
					_evs &= ~os::event_abort;
				}
			}
			if (_evs & os::event_write) break;
			if (_evs & os::event_close) return 0;
			if (_evs & os::event_err) throw filebuf_io_error();
			_evs |= fib::events_wait(_id, lock);
		}

		auto res = os::write(_file, buf);
		switch (res) {
		case os::file_closed: return 0;
		case os::would_block:
			_evs &= ~os::event_write;
			fib::events_report_ioblock(_id, os::event_write);
			break;
		default: {
			return res;
		}
		}
	}
}

std::size_t file_buf::write_until(const_buffer buf, time_point_type time) {
	for (std::unique_lock lock(_evs_mut);;) {
		for (;;) {
			if (clock_type::now() > time) throw socket_timeout_error();
			if (_evs & os::event_abort) {
				if (std::exchange(write_aborted, false)) {
					throw socket_aborted_error();
				} else if (!read_aborted) {
					_evs &= ~os::event_abort;
				}
			}
			if (_evs & os::event_write) break;
			if (_evs & os::event_close) return 0;
			if (_evs & os::event_err) throw filebuf_io_error();
			_evs |= fib::events_wait_until(_id, time, lock);
		}

		auto res = os::write(_file, buf);
		switch (res) {
		case os::file_closed: return 0;
		case os::would_block:
			_evs &= ~os::event_write;
			fib::events_report_ioblock(_id, os::event_write);
			break;
		default: return res;
		}
	}
}

file_buf::~file_buf() {
	if (_id != os::poll_id_type::invalid()) {
		fib::remove_file(std::exchange(_id, os::poll_id_type::invalid()));
		os::close(std::exchange(_file, nullptr));
	}
}

} // namespace polus