#include <polus/fiber/events.hpp>
#include <polus/io/io_buf.hpp>
#include <polus/log.hpp>

namespace polus {

constexpr std::size_t minimal_read_size = 8000;
static_assert(minimal_read_size < (io_buf::in_buffer_size / 2));

constexpr std::size_t minimal_write_size = 8000;
static_assert(minimal_write_size < (io_buf::out_buffer_size / 2));

io_buf::io_buf() noexcept {
	setp(_out_buffer.begin(), _out_buffer.end());
	setg(_in_buffer.begin(), _in_buffer.begin(), _in_buffer.begin());
}

io_buf::int_type io_buf::underflow() {
	if (gptr() != egptr()) return static_cast<int_type>(*gptr());
	if (gptr() == nullptr) return traits_type::eof();

	auto _read = read(_in_buffer);
	if (_read == 0) {
		setg(nullptr, nullptr, nullptr);
		return traits_type::eof();
	}
	setg(_in_buffer.begin(), _in_buffer.begin(), _in_buffer.begin() + _read);
	return *gptr();
}

std::streamsize io_buf::xsgetn(char_type *s, std::streamsize count) {
	mutable_buffer res(s, s + count);
	const_buffer current_data(gptr(), egptr());

	/* clearing data from streambufs internal buffer. */
	if (current_data.size() >= res.size()) {
		current_data = current_data.shrink(res.size());
		res << current_data;
		gbump(current_data.size());
		return count;
	} else {
		res = res << current_data;
		gbump(current_data.size());
	}

	/* reading directly into given memory block as long as it is larger than
	the minimal_read_size.*/
	fib::force_switch();
	while (res.size() > minimal_read_size) {
		auto r = read(res);
		if (r == 0) { return res.begin() - s; }
		res = res.shift(r);
	}

	/* After the given memory space is smaller than minimal_read_size we
	read to the streambuf's internal buffer. */
	while (res.size()) {
		if (!traits_type::not_eof(underflow())) return res.begin() - s;
		auto data = const_buffer(gptr(), egptr());
		if (data.size() > res.size()) [[likely]]
			data = data.shrink(res.size());
		res = res << data;
		gbump(data.size());
	}

	return count;
}

std::streamsize io_buf::xsputn(const char_type *s, std::streamsize count) {
	auto internal_start = pbase();
	auto internal_end = pptr();

	/* writing data from the internal buffer until there is less than
	 * minimal_write_size. */
	while (static_cast<std::size_t>(internal_end - internal_start) >=
		   minimal_write_size) {
		auto written = write(const_buffer(internal_start, internal_end));
		if (written == 0) {
			setp(nullptr, nullptr);
			return 0;
		}
		internal_start += written;
	}

	/* To prepare for appending data from the given pointer we need atleast
	 * minimal_write_size or count amount of space. */
	std::size_t to_internal_size =
		std::min<std::size_t>(minimal_write_size, count);
	if (static_cast<std::size_t>(epptr() - internal_end) < to_internal_size) {
		auto shift_back_size = internal_end - pbase();
		internal_start = std::copy(internal_start, internal_end, pbase());
		internal_end -= shift_back_size;
	}

	/* We add data from the given pointer and write the leftover data from
	the internal buffer along with this newly appended data. */
	auto added_end = internal_end;
	if (to_internal_size == minimal_write_size &&
		internal_end - internal_start) {
		added_end = std::copy(s, s + to_internal_size, internal_end);
	} else if (to_internal_size == static_cast<std::size_t>(count)) {
		added_end = std::copy(s, s + to_internal_size, internal_end);
		setp(internal_start, _out_buffer.end());
		pbump(added_end - internal_end);
		return count;
	}

	fib::force_switch();
	while (internal_start < internal_end) {
		auto written = write(const_buffer(internal_start, added_end));
		if (written == 0) {
			setp(nullptr, nullptr);
			return 0;
		}
		internal_start += written;
	}

	/* Then we write data directly from the given pointer. This is where the
	   true benefit of using sputn comes from as this saves us from copying the
	   data. */
	const_buffer sdata(s, s + count);
	sdata = sdata.shift(internal_start - internal_end);
	while (sdata.size() >= minimal_write_size) {
		auto res = write(sdata);
		if (res == 0) {
			setp(nullptr, nullptr);
			return sdata.begin() - s;
		}
		sdata = sdata.shift(res);
	}

	/* Lastly if there is some small amount of data left, which is not
	enough to waste a write call on, we copy this to the internal buffer. */
	std::copy(sdata.begin(), sdata.end(), _out_buffer.begin());
	setp(_out_buffer.begin(), _out_buffer.end());
	pbump(sdata.size());
	return count;
}

io_buf::int_type io_buf::overflow(int_type ch) {
	if (pptr() != epptr()) {
		if (traits_type::not_eof(ch)) {
			*pptr() = static_cast<char_type>(ch);
			pbump(1);
		}
		return static_cast<int_type>(*pptr());
	}
	if (pptr() == nullptr) return traits_type::eof();

	fib::force_switch();
	mutable_buffer data(pbase(), pptr());
	while (data.size() >= minimal_write_size) {
		auto res = write(data);
		if (res == 0) {
			setp(nullptr, nullptr);
			return traits_type::eof();
		}
		data = data.shift(res);
	}

	data = _out_buffer.get_passed(_out_buffer << data);
	setp(_out_buffer.begin(), _out_buffer.end());
	pbump(data.size());

	if (traits_type::not_eof(ch)) {
		*pptr() = static_cast<char_type>(ch);
		pbump(1);
	}

	return static_cast<char_type>(*pptr());
}

int io_buf::sync() {
	if (pptr() == nullptr) return traits_type::eof();

	fib::force_switch();
	mutable_buffer data(pbase(), pptr());
	while (data.size()) {
		auto res = write(data);
		if (res == 0) {
			setp(nullptr, nullptr);
			return traits_type::eof();
		}
		data = data.shift(res);
	}

	setp(_out_buffer.begin(), _out_buffer.end());
	return 0;
}

} // namespace polus