#include <polus/fiber/events.hpp>
#include <polus/io/file_buf.hpp>
#include <polus/io/tcp_listener.hpp>
#include <polus/log.hpp>

namespace polus {

tcp_listener::tcp_listener(const char *service)
	: _sock(os::open_tcp_listener(service)), _id(fib::register_file(_sock)),
	  _evs(os::event_read) {}

std::unique_ptr<socket_buf> tcp_listener::accept() {
	for (std::unique_lock lock(_evs_mut);;) {
		for (;;) {
			if (_evs & os::event_read) break;
			if (_evs & os::event_close) throw tcplistener_closed();
			if (_evs & os::event_err) throw tcplistener_io_error();
			if (_evs & os::event_abort) {
				_evs &= ~os::event_abort;
				throw listener_abort_error();
			}
			_evs |= fib::events_wait(_id, lock);
		}

		auto res = os::accept(_sock);
		if (res) { return std::make_unique<file_buf>(res); }
		_evs &= ~os::event_read;
		fib::events_report_ioblock(_id, os::event_read);
	}
}

std::unique_ptr<socket_buf> tcp_listener::accept_until(time_point_type time) {
	for (std::unique_lock lock(_evs_mut);;) {
		for (;;) {
			if (clock_type::now() > time) return nullptr;
			if (_evs & os::event_read) break;
			if (_evs & os::event_close) throw tcplistener_closed();
			if (_evs & os::event_err) throw tcplistener_io_error();
			if (_evs & os::event_abort) {
				_evs &= ~os::event_abort;
				throw listener_abort_error();
			}
			_evs |= fib::events_wait_until(_id, time, lock);
		}

		auto res = os::accept(_sock);
		if (res) { return std::make_unique<file_buf>(res); }
		_evs &= ~os::event_read;
		fib::events_report_ioblock(_id, os::event_read);
	}
}

void tcp_listener::abort_accept() { fib::abort_wait(_id); }

tcp_listener::~tcp_listener() {
	if (_sock != nullptr) {
		fib::remove_file(std::exchange(_id, os::poll_id_type::invalid()));
		os::close(std::exchange(_sock, nullptr));
	}
}

} // namespace polus