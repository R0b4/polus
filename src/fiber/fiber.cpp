#include <polus/fiber/condition_variable.hpp>
#include <polus/fiber/core.hpp>
#include <polus/fiber/mutex.hpp>
#include <polus/fiber/scheduler.hpp>
#include <polus/log.hpp>

namespace polus::fib {

void mutex::lock() {
	if (try_lock()) return;
	yield([this](fiber &&self) mutable {
		std::unique_lock __lock(_mut);
		if (std::exchange(_locked, true)) [[likely]] {
			_fibs.push(std::move(self));
		} else {
			set_ready(std::move(self));
		}
	});
}

bool mutex::try_lock() {
	std::lock_guard __lock(_mut);
	return !std::exchange(_locked, true);
}

void mutex::unlock() {
	std::lock_guard __lock(_mut);
	if (_locked = !_fibs.empty(); !_locked) return;
	set_ready(std::move(_fibs.front()));
	_fibs.pop();
}

void default_scheduler::notify_ready(fiber &&ready) {
	std::lock_guard __lock(_mut);
	fib_queue.push(std::move(ready));
	_cond.notify_one();
}

fiber default_scheduler::make_scheduled_fiber(ctx::fiber &&fiber) {
	std::lock_guard __lock(_mut);
	return std::make_pair(std::move(fiber), fibers.get());
}

fiber default_scheduler::get_fiber() {
	std::unique_lock __lock(_mut);
	while (fib_queue.empty()) {
		if (fibers.empty()) {
			for (auto &f : exit_handlers) f();
			exit_handlers.clear();
			_cond.notify_all();
			return std::make_pair<ctx::fiber, fiber_id_type>(ctx::fiber(), 0);
		}
		_cond.wait(__lock);
	}
	fiber res = std::move(fib_queue.front());
	fib_queue.pop();
	return res;
}

bool default_scheduler::has_ready_fibers() {
	std::unique_lock __lock(_mut);
	return !fib_queue.empty();
}

void default_scheduler::remove_fiber(fiber_id_type id) {
	std::lock_guard __lock(_mut);
	fibers.remove(id);
}

void default_scheduler::add_exit_handler(move_only_function<void()> &&func) {
	std::lock_guard __lock(_mut);
	exit_handlers.push_back(std::move(func));
}

namespace __impl {

struct FibItHash {
	std::size_t operator()(const std::list<fiber>::iterator &it) const {
		return std::hash<
			std::remove_reference_t<std::list<fiber>::iterator::reference> *>()(
			&*it);
	}
};

class TimingSchedular {
public:
	using fib_list = std::list<fiber>;
	using fib_iterator = fib_list::iterator;

	void wait(
		time_point_type time, fib_iterator fib, fib_list &lst,
		std::mutex &sig_mut) {
		if (_ordered_by_time.empty() || time < _ordered_by_time.begin()->first)
			_timers_changed.notify_one();
		_ordered_by_time[time].emplace_back(
			std::make_pair(fib, &lst), &sig_mut);
		_ordered_by_it.emplace(fib, time);
	}

	void cancel_wait(fib_iterator fib) {
		auto it = _ordered_by_it.find(fib);
		if (it == _ordered_by_it.end()) return;

		auto time = it->second;
		_ordered_by_it.erase(it);

		auto time_it = _ordered_by_time.find(time);
		auto &[_, vec] = *time_it;

		vec.erase(std::find_if(
			vec.begin(), vec.end(),
			[&fib](std::pair<std::pair<fib_iterator, fib_list *>, std::mutex *>
					   p) { return fib == p.first.first; }));

		if (vec.empty()) {
			if (time_it == _ordered_by_time.begin())
				_timers_changed.notify_one();
			_ordered_by_time.erase(time_it);
		}
	}

	void awaiting_thread() {
		fib::add_exit_handler([]() { TimingSchedular::get().terminate(); });
		while (true) {
			std::unique_lock<std::mutex> __lock(_time_mut);
			if (_ordered_by_time.empty()) _timers_changed.wait(__lock);
			else
				_timers_changed.wait_until(
					__lock, _ordered_by_time.begin()->first);
			if (_terminated) return;
			auto now = clock_type::now();
			for (auto time_it = _ordered_by_time.begin();
				 time_it != _ordered_by_time.end() && time_it->first <= now;
				 time_it = _ordered_by_time.begin()) {
				for (auto [fib, fib_mut] : time_it->second) {
					std::lock_guard __lock2(*fib_mut);
					auto [fib_it, lst] = fib;
					_ordered_by_it.erase(_ordered_by_it.find(fib_it));
					set_ready(std::move(*fib_it));
					lst->erase(fib_it);
				}
				_ordered_by_time.erase(time_it);
			}
		}
	}

	void terminate() {
		std::lock_guard<std::mutex> __lock(_time_mut);
		_terminated = true;
		_timers_changed.notify_one();
	}

	std::mutex _time_mut;
	static TimingSchedular &get() { return _instance; }

private:
	static TimingSchedular _instance;
	TimingSchedular() = default;
	std::condition_variable _timers_changed;
	std::map<
		time_point_type,
		std::vector<
			std::pair<std::pair<fib_iterator, fib_list *>, std::mutex *>>>
		_ordered_by_time;
	std::unordered_map<fib_iterator, time_point_type, FibItHash> _ordered_by_it;
	bool _terminated = false;
};

std::mutex &get_timing_scheduler_mutex() {
	return __impl::TimingSchedular::get()._time_mut;
}
void timing_scheduler_mutex_wait(
	time_point_type time, fib_iterator fib, fib_list &lst,
	std::mutex &sig_mut) {
	return __impl::TimingSchedular::get().wait(time, fib, lst, sig_mut);
}

TimingSchedular TimingSchedular::_instance;

} // namespace __impl

void condition_variable::notify_one() {
	{
		std::lock_guard __guard(_waiting_mut);
		if (_waiting.empty()) return;
	}
	std::lock_guard<std::mutex> __lock(
		__impl::TimingSchedular::get()._time_mut);
	std::lock_guard __guard(_waiting_mut);
	set_ready(std::move(_waiting.front()));
	__impl::TimingSchedular::get().cancel_wait(_waiting.begin());
	_waiting.pop_front();
}

void condition_variable::notify_all() {
	{
		std::lock_guard __guard(_waiting_mut);
		if (_waiting.empty()) return;
	}
	std::lock_guard __lock(__impl::TimingSchedular::get()._time_mut);
	std::lock_guard __guard(_waiting_mut);
	while (!_waiting.empty()) {
		set_ready(std::move(_waiting.front()));
		__impl::TimingSchedular::get().cancel_wait(_waiting.begin());
		_waiting.pop_front();
	}
}

void condition_timer() { __impl::TimingSchedular::get().awaiting_thread(); }

void sleep_until(const time_point_type &time) {
	fib::mutex _mut;
	std::unique_lock __lock(_mut);
	fib::condition_variable _var;
	_var.wait_until(__lock, time);
}

} // namespace polus::fib