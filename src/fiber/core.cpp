#include <polus/fiber/core.hpp>
#include <polus/utils/exception.hpp>

namespace polus::fib {

namespace __impl {

inline std::unique_ptr<scheduler> used_scheduler = nullptr;
class fiber_switch {
public:
	static fiber_switch &get() {
		thread_local fiber_switch instance;
		return instance;
	}

	fiber_id_type current_fiber;
	ctx::fiber switching_fiber;
	move_only_function<void(fiber &&)> fiber_callback;

private:
	fiber_switch() noexcept = default;
};

} // namespace __impl

void add_exit_handler(move_only_function<void()> &&func) {
	__impl::used_scheduler->add_exit_handler(std::move(func));
}

void set_scheduler(std::unique_ptr<scheduler> &&scheduler) {
	__impl::used_scheduler = std::move(scheduler);
}

fiber_id_type current_fiber() {
	return __impl::fiber_switch::get().current_fiber;
}

void set_ready(fiber &&fib) {
	__impl::used_scheduler->notify_ready(std::move(fib));
}

void spawn(move_only_function<void()> func, std::size_t stack_size) {
	ctx::fixedsize_stack stk(stack_size);
	set_ready(__impl::used_scheduler->make_scheduled_fiber(ctx::fiber(
		std::allocator_arg, std::move(stk),
		[func = std::move(func)](ctx::fiber &&self) mutable noexcept {
			__impl::fiber_switch::get().switching_fiber = std::move(self);
			func();
			__impl::used_scheduler->remove_fiber(current_fiber());
			return std::move(__impl::fiber_switch::get().switching_fiber);
		})));
}

void yield(move_only_function<void(fiber &&)> &&handle_self) {
	if (__impl::fiber_switch::get().fiber_callback) throw polus_exception();
	__impl::fiber_switch::get().fiber_callback = std::move(handle_self);
	__impl::fiber_switch::get().switching_fiber =
		std::move(__impl::fiber_switch::get().switching_fiber).resume();
}

void force_switch() {
	if (!__impl::used_scheduler->has_ready_fibers()) return;
	yield([](fiber &&self) { set_ready(std::move(self)); });
}

void fiber_thread_loop() {
	while (true) {
		fiber to_run = __impl::used_scheduler->get_fiber();
		if (!to_run.first) return;
		__impl::fiber_switch::get().current_fiber = to_run.second;
		fiber after = std::make_pair(
			std::move(to_run.first).resume(),
			__impl::fiber_switch::get().current_fiber);
		if (after.first && __impl::fiber_switch::get().fiber_callback) {
			__impl::fiber_switch::get().fiber_callback(std::move(after));
			__impl::fiber_switch::get().fiber_callback = nullptr;
		}
	}
}

} // namespace polus::fib