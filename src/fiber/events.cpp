#include <polus/fiber/condition_variable.hpp>
#include <polus/fiber/events.hpp>
#include <polus/fiber/mutex.hpp>
#include <polus/utils/id_map.hpp>
#include <unordered_set>

namespace polus::fib {

struct __file_data {
	__file_data(os::file *f) : file(f) {}
	os::file *file;
	os::events_mask events = os::no_events;
	fib::condition_variable var;
};

struct __events {
	static __events &get() { return _instance; }

	bool terminate;
	os::poll *poll;
	std::mutex mutex;
	std::unordered_set<os::poll_id_type> to_remove;
	id_map<os::poll_id_type, std::unique_ptr<__file_data>> data;

private:
	__events() : terminate(false), poll(os::make_poll()) {}
	static __events _instance;
};
__events __events::_instance;

static __events &eget() { return __events::get(); }

os::poll_id_type register_file(os::file *f) {
	std::lock_guard __lock(eget().mutex);
	auto id = eget().data.add(std::make_unique<__file_data>(f));
	os::poll_add(eget().poll, f, id);
	return id;
}

void events_report_ioblock(os::poll_id_type id, os::events_mask evs) {
	std::lock_guard __lock(eget().mutex);
	os::poll_report_ioblock(eget().poll, id, evs);
}

os::events_mask events_wait(os::poll_id_type id) {
	std::unique_lock __lock(eget().mutex);
	auto &data = *eget().data[id];
	data.var.wait(__lock, [&data] { return data.events != os::no_events; });
	return std::exchange(data.events, os::no_events);
}

os::events_mask events_wait_until(os::poll_id_type id, time_point_type time) {
	std::unique_lock __lock(eget().mutex);
	auto &data = *eget().data[id];
	data.var.wait_until(
		__lock, time, [&data] { return data.events != os::no_events; });
	return std::exchange(data.events, os::no_events);
}

os::events_mask
events_wait(os::poll_id_type id, std::unique_lock<fib::mutex> &lock) {
	std::unique_lock __lock(eget().mutex);
	lock.unlock();
	auto &data = *eget().data[id];
	data.var.wait(__lock, [&data] { return data.events != os::no_events; });
	lock.lock();
	return std::exchange(data.events, os::no_events);
}

os::events_mask events_wait_until(
	os::poll_id_type id, time_point_type time,
	std::unique_lock<fib::mutex> &lock) {
	std::unique_lock __lock(eget().mutex);
	lock.unlock();
	auto &data = *eget().data[id];
	data.var.wait_until(
		__lock, time, [&data] { return data.events != os::no_events; });
	lock.lock();
	return std::exchange(data.events, os::no_events);
}

void abort_wait(os::poll_id_type id) {
	std::lock_guard __lock(eget().mutex);
	auto &data = *eget().data[id];
	data.events |= os::event_abort;
	data.var.notify_one();
}

void remove_file(os::poll_id_type id) {
	std::lock_guard __lock(eget().mutex);
	auto &data = *eget().data[id];
	data.events |= os::event_abort;
	data.var.notify_one();
	os::poll_remove(eget().poll, data.file, id);
	eget().to_remove.insert(id);
}

void events_wait_loop() {
	fib::add_exit_handler([]() {
		eget().terminate = true;
		auto [out, in] = os::make_pipe();
		close(out);
		events_report_ioblock(register_file(in), os::event_write);
	});
	while (true) {
		os::poll_wait(eget().poll);
		std::lock_guard __lock(eget().mutex);
		if (eget().terminate) return;
		while (true) {
			auto [id, events] = os::poll_get_event(eget().poll);
			if (eget().to_remove.contains(id)) continue;
			if (events == os::no_events) break;
			auto &data = *eget().data[id];
			data.events |= events;
			data.var.notify_all();
		}
		for (auto id : eget().to_remove) { eget().data.erase(id); }
		eget().to_remove.clear();
	}
}

} // namespace polus::fib