#include <polus/fiber/all.hpp>
#include <polus/http/http1.hpp>
#include <polus/io/tcp_listener.hpp>

namespace fib = polus::fib;

void client(std::unique_ptr<polus::socket_buf> sock) {
	static std::vector<char> dat(10000000, 'a');
	if (sock == nullptr) return;
	auto _buf = std::make_unique<polus::http1_buf>(*sock, true);
	_buf->write_top(polus::http_version::v11, 200, "OK");
	_buf->write_header(polus::header("transfer-encoding", "chunked"));
	while (static_cast<std::size_t>(_buf->sputn(dat.data(), dat.size())) ==
		   dat.size()) {}
	_buf->close_write_side();
	_buf->pubsync();
}

void listen() {
	for (polus::tcp_listener listener("1234");;)
		fib::async(client, listener.accept());
}

int main() { fib::run_fiber_main(listen); }