BUILD_FOLDER := build
CODE_FOLDERS := src tests include app

CLANG_FORMAT_FILE := .clang-format
FORMAT_LOGS_FOLDER := formatted

CMAKE_PROCESSES := 20

.PHONY: all
all: build test


.PHONY: build
build: format
	mkdir -p $(BUILD_FOLDER) 
	cmake -B $(BUILD_FOLDER) -DCMAKE_BUILD_TYPE=Release
	cmake --build $(BUILD_FOLDER) -j $(CMAKE_PROCESSES)


.PHONY: test
test: build
	build/polus_test


.PHONY: clean
clean:
	rm -rf $(BUILD_FOLDER)
	mkdir -p $(BUILD_FOLDER)


CODE_FILES := $(shell find $(CODE_FOLDERS) -not -type d)
FORMAT_LOG_FILES := $(CODE_FILES:%=$(BUILD_FOLDER)/$(FORMAT_LOGS_FOLDER)/%.log)
$(BUILD_FOLDER)/$(FORMAT_LOGS_FOLDER)/%.log: % $(CLANG_FORMAT_FILE)
	clang-format -style=file:$(CLANG_FORMAT_FILE) -i $<
	@mkdir -p $(dir $@)
	@touch $@
.PHONY: format
format: $(FORMAT_LOG_FILES)


.PHONY: count_lines
count_lines:
	@find . -wholename './src/*.*' -or -wholename './include/*.*' | sed 's/.*/"&"/' | xargs  wc -l
