#include <polus/fiber/all.hpp>
#include <polus/http/http1.hpp>
#include <polus/testing/debug_buf.hpp>
#include <polus/testing/testing.hpp>
#include <polus/testing/tests.hpp>

namespace polus::testing {

static void test_chunked_transfer() {
	auto [receiver, sender] = make_shared_buf_pair();

	std::vector<char> received;
	std::vector<char> check;

	const const_buffer data = "1234567890";
	const std::size_t send_amount = 123456;

	auto recvfut = fib::async(sizeof(http1_buf) + 10000, [&] {
		auto receiverl = std::move(receiver);
		http1_buf _buf(*receiverl.get(), false);
		std::ranges::copy(
			std::istreambuf_iterator(&_buf), std::default_sentinel,
			std::back_inserter(received));
	});

	auto sendfut = fib::async(sizeof(http1_buf) + 10000, [&] {
		auto senderl = std::move(sender);
		http1_buf _buf(*senderl.get(), true);
		_buf.write_top(http_version::v11, 200, "OK");
		_buf.emplace_header("transfer-encoding", "chunked");
		auto it = std::ostreambuf_iterator(&_buf);
		for (std::size_t i = 0; i < send_amount; ++i) {
			std::ranges::copy(data, it);
			std::ranges::copy(data, std::back_inserter(check));
		}
		_buf.close_write_side();
		_buf.pubsync();
	});

	recvfut.get();
	sendfut.get();
	POLUS_ASSERT(check.size() == received.size());
	POLUS_ASSERT(received == check);
}

static void test_serial_transfer() {
	auto [receiver, sender] = make_shared_buf_pair();

	std::vector<char> received;
	std::vector<char> check;

	const const_buffer data = "1234567890";
	const std::size_t send_amount = 123456;

	auto recvfut = fib::async(sizeof(http1_buf) + 8000, [&] {
		auto receiverl = std::move(receiver);
		http1_buf _buf(*receiverl.get(), false);
		std::ranges::copy(
			std::istreambuf_iterator(&_buf), std::default_sentinel,
			std::back_inserter(received));
	});

	auto sendfut = fib::async(sizeof(http1_buf) + 8000, [&] {
		auto senderl = std::move(sender);
		http1_buf _buf(*senderl.get(), true);
		_buf.write_top(http_version::v11, 200, "OK");
		_buf.emplace_header("content-length", "1234560");
		auto it = std::ostreambuf_iterator(&_buf);
		for (std::size_t i = 0; i < send_amount; ++i) {
			std::ranges::copy(data, it);
			std::ranges::copy(data, std::back_inserter(check));
		}
		_buf.close_write_side();
		_buf.pubsync();
	});

	recvfut.get();
	sendfut.get();
	POLUS_ASSERT(check.size() == received.size());
	POLUS_ASSERT(received == check);
}

static void test_http1_chunked_putgetn() {
	auto [receiver, sender] = make_shared_buf_pair();

	std::vector<char> received;
	std::vector<char> write_block;

	const const_buffer data = "1234567890";
	const std::size_t send_amount = 123456;

	for (std::size_t i = 0; i < send_amount; ++i)
		std::ranges::copy(data, std::back_inserter(write_block));

	auto read_future = fib::async(sizeof(http1_buf) + 8000, [&] {
		auto receiverl = std::move(receiver);
		http1_buf _buf(*receiverl.get(), false);
		received.resize(write_block.size());
		std::size_t _read = _buf.sgetn(received.data(), received.size());
		POLUS_ASSERT(_read == received.size());
	});

	auto write_future = fib::async(sizeof(http1_buf) + 8000, [&] {
		auto senderl = std::move(sender);
		http1_buf _buf(*senderl.get(), true);
		_buf.write_top(http_version::v11, 200, "OK");
		_buf.emplace_header("transfer-encoding", "chunked");
		std::size_t written =
			_buf.sputn(write_block.data(), write_block.size());
		_buf.close_write_side();
		_buf.pubsync();
		POLUS_ASSERT(written == write_block.size());
	});

	read_future.get();
	write_future.get();
	POLUS_ASSERT(write_block.size() == received.size());
	POLUS_ASSERT(received == write_block);
}

static void test_http1_serial_putgetn() {
	auto [receiver, sender] = make_shared_buf_pair();

	std::vector<char> received;
	std::vector<char> write_block;

	const const_buffer data = "1234567890";
	const std::size_t send_amount = 123456;

	for (std::size_t i = 0; i < send_amount; ++i)
		std::ranges::copy(data, std::back_inserter(write_block));

	auto read_future = fib::async(sizeof(http1_buf) + 8000, [&] {
		auto receiverl = std::move(receiver);
		http1_buf _buf(*receiverl.get(), false);
		received.resize(write_block.size());
		std::size_t _read = _buf.sgetn(received.data(), received.size());
		POLUS_ASSERT(_read == received.size());
	});

	auto write_future = fib::async(sizeof(http1_buf) + 8000, [&] {
		auto senderl = std::move(sender);
		http1_buf _buf(*senderl.get(), true);
		_buf.write_top(http_version::v11, 200, "OK");
		_buf.emplace_header("content-length", "1234560");
		std::size_t written =
			_buf.sputn(write_block.data(), write_block.size());
		_buf.close_write_side();
		_buf.pubsync();
		POLUS_ASSERT(written == write_block.size());
	});

	read_future.get();
	write_future.get();
	POLUS_ASSERT(write_block.size() == received.size());
	POLUS_ASSERT(received == write_block);
}

void test_http() {
	POLUS_TESTF(
		"Test http1 chunked transfer with stream iterators",
		test_chunked_transfer);
	POLUS_TESTF(
		"Test http1 serial transfer with stream iterators",
		test_serial_transfer);
	POLUS_TESTF(
		"Test http1 chunked transfer with putn and getn",
		test_http1_chunked_putgetn);
	POLUS_TESTF(
		"Test http1 serial transfer with putn and getn",
		test_http1_serial_putgetn);
}

} // namespace polus::testing
