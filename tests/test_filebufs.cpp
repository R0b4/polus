#include <polus/fiber/all.hpp>
#include <polus/io/file_buf.hpp>
#include <polus/os/file.hpp>
#include <polus/testing/testing.hpp>
#include <polus/testing/tests.hpp>

namespace polus::testing {

static void test_filebufs_iterator() {
	auto [outp, inp] = os::make_pipe();

	std::vector<char> received;
	std::vector<char> check;

	const const_buffer data = "1234567890";
	const std::size_t send_amount = 123456;

	auto read_future = fib::async([&] {
		auto _buf = std::make_unique<file_buf>(outp);
		std::ranges::copy(
			std::istreambuf_iterator(_buf.get()), std::default_sentinel,
			std::back_inserter(received));
	});

	auto write_future = fib::async([&] {
		auto _buf = std::make_unique<file_buf>(inp);
		auto it = std::ostreambuf_iterator(_buf.get());
		for (std::size_t i = 0; i < send_amount; ++i) {
			std::ranges::copy(data, it);
			std::ranges::copy(data, std::back_inserter(check));
		}
		_buf->pubsync();
	});

	read_future.get();
	write_future.get();
	POLUS_ASSERT(check.size() == received.size());
	POLUS_ASSERT(received == check);
}

static void test_filebufs_putgetn() {
	auto [outp, inp] = os::make_pipe();

	std::vector<char> received;
	std::vector<char> write_block;

	const const_buffer data = "1234567890";
	const std::size_t send_amount = 123456;

	for (std::size_t i = 0; i < send_amount; ++i)
		std::ranges::copy(data, std::back_inserter(write_block));

	auto read_future = fib::async([&] {
		auto _buf = std::make_unique<file_buf>(outp);
		received.resize(write_block.size());
		std::size_t _read = _buf->sgetn(received.data(), received.size());
		POLUS_ASSERT(_read == received.size());
	});

	auto write_future = fib::async([&] {
		auto _buf = std::make_unique<file_buf>(inp);
		std::size_t written =
			_buf->sputn(write_block.data(), write_block.size());
		_buf->pubsync();
		POLUS_ASSERT(written == write_block.size());
	});

	read_future.get();
	write_future.get();
	POLUS_ASSERT(write_block.size() == received.size());
	POLUS_ASSERT(received == write_block);
}

static void test_filebufs_abort() {
	auto [outp, inp] = os::make_pipe();

	auto out_buf = std::make_unique<file_buf>(outp);
	auto in_buf = std::make_unique<file_buf>(inp);

	auto read_future = fib::async([&] { out_buf->sgetc(); });
	out_buf->read_abort();
	try {
		read_future.get();
		POLUS_ASSERT(false);
	} catch (socket_aborted_error &) {}

	auto write_future = fib::async([&] {
		std::ranges::fill_n(
			std::ostreambuf_iterator(in_buf.get()), 100000, ' ');
	});
	in_buf->write_abort();
	try {
		write_future.get();
		POLUS_ASSERT(false);
	} catch (socket_aborted_error &) {}
}

void test_filebufs() {
	POLUS_TESTF("Test filebufs with stream iterators", test_filebufs_iterator);
	POLUS_TESTF("Test filebufs with sputn and sgetn", test_filebufs_putgetn);
	POLUS_TESTF("Test filebufs abort", test_filebufs_abort);
};

} // namespace polus::testing