#include <polus/buffer.hpp>
#include <polus/fiber/all.hpp>
#include <polus/testing/testing.hpp>
#include <polus/testing/tests.hpp>
#include <thread>

namespace polus::testing {

void run_all_tests() noexcept {
	POLUS_TESTF("file bufs", test_filebufs);
	POLUS_TESTF("debug bufs", test_debugbufs);
	POLUS_TESTF("http1", test_http);
}

} // namespace polus::testing

int main() { polus::fib::run_fiber_main(polus::testing::run_all_tests); }
